# makefile -- makefile for galois.
# Copyright (C) 2011-2014 Gerardo Ballabio
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

AC = config.log config.status autom4te.cache
BAK = *~

default:
	cd src ; make

# documentation
html :
	cd doc ; make html
pdf :
	cd doc ; make pdf

# other targets
clean :
	- rm -rf ${BAK}
	- cd src ; make clean
	- cd doc ; make clean
	- cd misc ; make clean
distclean : clean
	- rm -rf ${AC}
	- cd src ; make distclean
	- cd doc ; make distclean
	- cd misc ; make distclean

install :
	cd src ; make install
	cd doc ; make install
	cd misc ; make install
uninstall :
	- cd src ; make uninstall
	- cd doc ; make uninstall
	- cd misc ; make uninstall

