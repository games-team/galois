/* area.cc -- game widgets for falling blocks game.  -*- C++ -*-
   Copyright (C) 2011-2014 Gerardo Ballabio

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#include <cmath>
#include <gtkmm.h>
#include "area.h"
#include "grid.h"
#include "group.h"

void BoardArea::draw_2d(Cairo::RefPtr<Cairo::Context> context)
{
  // draw board
  g->draw_board(context, cells->get_rows(), cells->get_cols(), 1, brick_size);

  // draw fallen bricks
  for (int i=0; i<cells->get_rows(); ++i)
    for (int j=0; j<cells->get_cols(); ++j)
      {
	coords c = cells->get_c0() + make_coords(j, i);
	int n = cells->get(c);
	if (n > 0)
	  g->draw_brick(context, c, colors[n % ncolors], brick_size);
      }

  // draw shadow of active block
  for (int i=0; i<shadow.get_rows(); ++i)
    for (int j=0; j<shadow.get_cols(); ++j)
      {
	coords c = shadow.get_c0() + make_coords(j, i);
	if (shadow.get(c))
	  g->draw_shadow(context, c, colors[iblock], brick_size);
      }

  // draw active block
  for (int i=0; i<block.get_rows(); ++i)
    for (int j=0; j<block.get_cols(); ++j)
      {
	coords c = block.get_c0() + make_coords(j, i);
	if (block.get(c))
	  g->draw_brick(context, c, colors[iblock], brick_size);
      }
}

void BoardArea::draw_3d(Cairo::RefPtr<Cairo::Context> context)
{
  const group_3d *g3 = dynamic_cast<const group_3d *>(g);
  if (!g3)
    return;

  // draw board
  g->draw_board(context, cells->get_rows(), cells->get_cols(),
		cells->get_layers(), brick_size);

  // draw bottom planes first
  for (int i=cells->get_rows()-1; i>=0; --i)
    {
      // color each plane differently
      const int offset = 10;
      int n = (i + offset) % ncolors;

      // draw plane which frames block
      int y = cells->get_y0() + i;
      if (y == block.get_y0() + block.get_rows() / 2)
	g3->draw_plane(context, y, cells->get_cols(), cells->get_layers(),
		       brick_size);

      for (int j=0; j<cells->get_layers(); ++j)
	for (int k=0; k<cells->get_cols(); ++k)
	  {

	    // draw fallen bricks
	    coords c = cells->get_c0() + make_coords(k, i, j);
	    int nn = cells->get(c);
	    if (nn > 0)
	      g->draw_brick(context, c, colors[n], brick_size);
	    else if (nn < 0)
	      g->draw_brick(context, c, colors[-nn % ncolors], brick_size);

	    // draw shadow of active block
	    if (shadow.is_in(c) && shadow.get(c))
	      g->draw_shadow(context, c, colors[n], brick_size);

	    // draw active block
	    if (block.is_in(c) && block.get(c))
	      g->draw_brick(context, c, colors[n], brick_size);
	  }
    }

  // draw board
  g3->draw_board_front(context, cells->get_rows(), cells->get_cols(),
		       cells->get_layers(), brick_size);
}

void BoardArea::draw_pause_message(Cairo::RefPtr<Cairo::Context> context)
{
  // center of board
  Gdk::Rectangle rectangle =
    g->board_size(brick_size, cells->get_rows(), cells->get_cols(),
		  cells->get_layers());
  int cx = rectangle.get_x() + rectangle.get_width() / 2,
    cy = rectangle.get_y() + rectangle.get_height() / 2;

  // text dimensions
  Glib::RefPtr<Pango::Layout> layout1 = create_pango_layout("Game paused"),
    layout2 = create_pango_layout(Glib::ustring("Press ") + pause_key +
				  " to resume");
  Pango::Rectangle
    extent1 = layout1->get_line(0)->get_pixel_logical_extents(),
    extent2 = layout2->get_line(0)->get_pixel_logical_extents();
  int w1 = (extent1.get_width() - extent1.get_lbearing()),
    w2 = (extent2.get_width() - extent2.get_lbearing()),
    h1 = extent1.get_height(), h2 = extent2.get_descent(),
    h3 = (extent1.get_descent() + extent2.get_height());

  // draw with extra thickness and inverted color as background
  context->set_source_rgba(0.0, 0.0, 0.0, 1.0);
  context->set_line_width(4.0);
  context->move_to(cx - w1 / 2, cy + (h1 - h2 - h3) / 2);
  layout1->get_line(0)->add_to_cairo_context(context);
  context->move_to(cx - w2 / 2, cy + (h1 - h2 + h3) / 2);
  layout2->get_line(0)->add_to_cairo_context(context);
  context->stroke();

  // draw as foreground
  context->set_source_rgba(1.0, 1.0, 1.0, 1.0);
  context->move_to(cx - w1 / 2, cy + (h1 - h2 - h3) / 2);
  layout1->get_line(0)->show_in_cairo_context(context);
  context->move_to(cx - w2 / 2, cy + (h1 - h2 + h3) / 2);
  layout2->get_line(0)->show_in_cairo_context(context);
}

void BoardArea::draw_game_over_message(Cairo::RefPtr<Cairo::Context> context)
{
  // text dimensions
  Glib::RefPtr<Pango::Layout> layout = create_pango_layout("Game over");
  Pango::Rectangle extent = layout->get_line(0)->get_pixel_logical_extents();
  int w = (extent.get_width() - extent.get_lbearing()),
    h1 = extent.get_height(), h2 = extent.get_descent();

  // center of board
  Gdk::Rectangle rectangle =
    g->board_size(brick_size, cells->get_rows(), cells->get_cols(),
		  cells->get_layers());
  int cx = (rectangle.get_x() + rectangle.get_width() / 2),
    cy = (rectangle.get_y() + rectangle.get_height() / 2);

  // draw large font, but make sure that it fits within board
  double scale = 2.0;
  if (w * scale > rectangle.get_width())
    scale = rectangle.get_width() / w;
  context->scale(scale, scale);

  // draw with extra thickness and inverted color as background
  context->set_source_rgba(0.0, 0.0, 0.0, 1.0);
  context->set_line_width(5.0 / scale);
  context->move_to(cx / scale - w / 2, cy / scale + (h1 - h2) / 2);
  layout->get_line(0)->add_to_cairo_context(context);
  context->stroke();

  // draw as foreground
  context->set_source_rgba(1.0, 1.0, 1.0, 1.0);
  context->move_to(cx / scale - w / 2, cy / scale + (h1 - h2) / 2);
  layout->get_line(0)->show_in_cairo_context(context);
}

bool BoardArea::on_draw(const Cairo::RefPtr<Cairo::Context> &context)
{
  if (!g || !cells)
    return true;

  Glib::RefPtr<Gdk::Window> window = get_window();
  if (!window)
    return false;

  // set largest brick size that fits within widget (must be even)
  for (brick_size=min_brick_size+2; brick_size<1000; brick_size+=2)
    {
      Gdk::Rectangle rectangle =
	g->board_size(brick_size, cells->get_rows(), cells->get_cols(),
		      cells->get_layers());
      if (rectangle.get_width() > get_width() ||
	  rectangle.get_height() > get_height())
	break;
    }
  brick_size -= 2;

  // set coordinate system
  Gdk::Rectangle rectangle =
    g->board_size(brick_size, cells->get_rows(), cells->get_cols(),
		  cells->get_layers());
  // align to the right, top
  // offset by half pixel to create sharp edges
  context->translate(-rectangle.get_x() + get_width() -
		     rectangle.get_width() + 0.5, -rectangle.get_y() + 0.5);

  // draw things
  if (g->is_3d())
    draw_3d(context);
  else
    draw_2d(context);

  if (pause)
    draw_pause_message(context);
  if (over)
    draw_game_over_message(context);

  return true;
}

void BoardArea::start_game(const group *gg, const grid<int> *c,
			   const grid<bool> &b, int i)
{
  g = gg;
  cells = c;
  block = b;
  iblock = i % ncolors;
  pause = over = false;

  // set minimum widget size
  Gdk::Rectangle rectangle =
    g->board_size(min_brick_size, cells->get_rows(), cells->get_cols(),
		  cells->get_layers());
  set_size_request(rectangle.get_width(), rectangle.get_height());
}

void BoardArea::set_block(const grid<bool> &b, const grid<bool> &s, int i)
{
  block = b;
  iblock = i % ncolors;
  shadow = s;

  queue_draw();
}

void NextArea::draw_2d(Cairo::RefPtr<Cairo::Context> context)
{
  for (int i=0; i<block.get_rows(); ++i)
    for (int j=0; j<block.get_cols(); ++j)
      {
	coords c = block.get_c0() + make_coords(j, i);
	if (block.get(c))
	  g->draw_brick(context, c, colors[nblock], brick_size);
      }
}

void NextArea::draw_3d(Cairo::RefPtr<Cairo::Context> context)
{
  // draw bottom planes first
  for (int i=block.get_rows()-1; i>=0; --i)
    {
      // color each plane differently
      const int offset = 10;
      int n = (i + offset) % ncolors;

      for (int j=0; j<block.get_layers(); ++j)
	for (int k=0; k<block.get_cols(); ++k)
	  {
	    coords c = block.get_c0() + make_coords(k, i, j);
	    if (block.get(c))
	      g->draw_brick(context, c, colors[n], brick_size);
	  }
    }
}

bool NextArea::on_draw(const Cairo::RefPtr<Cairo::Context> &context)
{
  if (!g)
    return true;

  Glib::RefPtr<Gdk::Window> window = get_window();
  if (!window)
    return false;

  // set brick size
  if (b)
    brick_size = b->get_brick_size();
  else
    brick_size = min_brick_size;

  // draw background
  context->set_source_rgb(0.0, 0.0, 0.0);
  context->rectangle(0, 0, width * brick_size, height * brick_size);
  context->fill();

  // set coordinate system
  // offset by half pixel to create sharp edges
  Gdk::Rectangle rectangle = g->block_size(block, brick_size);
  context->translate
    (-rectangle.get_x() +
     (width * brick_size - rectangle.get_width()) / 2 + 0.5,
     -rectangle.get_y() +
     (height * brick_size - rectangle.get_height()) / 2 + 0.5);

  // draw block
  if (g->is_3d())
    draw_3d(context);
  else
    draw_2d(context);

  return true;
}

void NextArea::set_block(const grid<bool> &b, int n)
{
  block = b;
  nblock = n % ncolors;

  // increase widget size if needed
  Gdk::Rectangle rectangle = g->block_size(block, min_brick_size);
  if (rectangle.get_width() > width * min_brick_size ||
      rectangle.get_height() > height * min_brick_size)
    {
      int w = static_cast<int>(std::ceil(double(rectangle.get_width()) /
					 min_brick_size)),
	h = static_cast<int>(std::ceil(double(rectangle.get_height()) /
				       min_brick_size));
      if (width < w)
	width = w;
      if (height < h)
	height = h;
      set_size_request(width * min_brick_size, height * min_brick_size);
    }

  queue_draw();
}
