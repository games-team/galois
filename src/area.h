/* area.h -- game widgets for falling blocks game.  -*- C++ -*-
   Copyright (C) 2011-2015 Gerardo Ballabio

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#ifndef __AREA_
#define __AREA_

#include <vector>
#include <gtkmm.h>
#include "grid.h"
#include "group.h"

const Gdk::RGBA colors[] = {
  Gdk::RGBA("#b0ff00"), // yellow-green
  Gdk::RGBA("#ffa0a0"), // pink
  Gdk::RGBA("#b060ff"), // violet
  Gdk::RGBA("#00ffb0"), // green-blue
  Gdk::RGBA("#ff40a0"), // purple-red
  Gdk::RGBA("#40a0ff"), // light blue
  Gdk::RGBA("#ffff00"), // yellow
  Gdk::RGBA("#00ff00"), // green
  Gdk::RGBA("#ff4040"), // red
  Gdk::RGBA("#00ffff"), // cyan
  Gdk::RGBA("#ff00ff"), // purple
  Gdk::RGBA("#ffa040"), // orange
  Gdk::RGBA("#8080ff"), // blue-violet
  Gdk::RGBA("#c08080"), // brown
  Gdk::RGBA("#b0b040"), // yellow-brown
};
const int ncolors = sizeof(colors) / sizeof(colors[0]);
const int min_brick_size = 24;

class BoardArea : public Gtk::DrawingArea
{
private:
  const group *g;
  const grid<int> *cells;
  grid<bool> block, shadow;
  int iblock;
  int brick_size; // unit of measure for drawing
  bool pause, over;
  Glib::ustring pause_key;

  void draw_2d(Cairo::RefPtr<Cairo::Context> context);
  void draw_3d(Cairo::RefPtr<Cairo::Context> context);
  void draw_pause_message(Cairo::RefPtr<Cairo::Context> context);
  void draw_game_over_message(Cairo::RefPtr<Cairo::Context> context);
protected:
  virtual bool on_draw(const Cairo::RefPtr<Cairo::Context> &context);
public:
  BoardArea() : g(0), cells(0), brick_size(min_brick_size),
		pause(false), over(false)
  { set_size_request(brick_size * 12, brick_size * 21); }

  int get_brick_size() const { return brick_size; }

  void start_game(const group *gg, const grid<int> *c, const grid<bool> &b,
		  int i);
  void end_game() { over = true; queue_draw(); }
  void set_block(const grid<bool> &b, const grid<bool> &s, int i);
  void set_pause(bool p) { pause = p; queue_draw(); }
  void set_pause_key(const Glib::ustring &key) { pause_key = key; }
};

class NextArea : public Gtk::DrawingArea
{
  const group *g;
  grid<bool> block;
  int nblock;
  int brick_size; // unit of measure for drawing
  int width, height;
  const BoardArea *b;

  void draw_2d(Cairo::RefPtr<Cairo::Context> context);
  void draw_3d(Cairo::RefPtr<Cairo::Context> context);
protected:
  virtual bool on_draw(const Cairo::RefPtr<Cairo::Context> &context);
public:
  NextArea() : g(0), brick_size(min_brick_size), width(4), height(4), b(0)
  { set_size_request(width * min_brick_size, height * min_brick_size); }

  void set_group(const group *gg) { g = gg; }
  void set_block(const grid<bool> &b, int n);
  void set_board_area(const BoardArea *bb) { b = bb; }
};

#endif /* __AREA_ */
