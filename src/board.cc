/* board.cc -- game board for falling blocks game.  -*- C++ -*-
   Copyright (C) 2011-2020 Gerardo Ballabio

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#include <cstdlib>
#include <vector>
#include <gtkmm.h>
#include "board.h"
#include "grid.h"
#include "group.h"

#include <iostream>
#include <iomanip>

bool game_board::fits(const block &b, int s, coords c) const
{
  std::vector<coords> bricks = get_bricks(b.shape[s]);
  for (std::vector<coords>::const_iterator i=bricks.begin();
       i!=bricks.end(); ++i)
    {
      coords n = c + *i;
      if (!cells.is_in(n) || cells.get(n))
	return false;
    }

  return true;
}

bool game_board::fits_b(const block &b, int s, coords c) const
{
  std::vector<coords> bricks = get_bricks(b.blocker[s]);
  for (std::vector<coords>::const_iterator i=bricks.begin();
       i!=bricks.end(); ++i)
    {
      coords n = c + *i;
      if (!cells.is_in(n) || cells.get(n))
	return false;
    }

  return true;
}

coords game_board::move_in(const block &b, int s, coords c) const
{
  // move down if necessary
  while (b.shape[s].get_y0() + c.y < cells.get_y0())
    c = g->down(c);

  // move right if necessary
  while (b.shape[s].get_x0() + c.x < cells.get_x0())
    c = g->right(c);

  // move left if necessary
  while (b.shape[s].get_x0() + b.shape[s].get_cols() + c.x >
	 cells.get_x0() + cells.get_cols())
    c = g->left(c);

  // move back if necessary
  while (b.shape[s].get_z0() + c.z < cells.get_z0())
    c = g->back(c);

  // move front if necessary
  while (b.shape[s].get_z0() + b.shape[s].get_layers() + c.z >
	 cells.get_z0() + cells.get_layers())
    c = g->front(c);

  return c;
}

void game_board::first_block()
{
  nsize = bmax;
  // choose block
  nindex = rand() % series[nsize];
  for (int i=0; i<nsize; ++i)
    nindex += series[i];
  nblock = blocks[nindex];
  // choose orientation
  nshape = rand() % nblock.shape.size();

  // move to active block
  next_block();
}

void game_board::land_block()
{
  // dump block onto cells grid
  std::vector<coords> bricks = get_bricks(fblock.shape[fshape]);
  for (std::vector<coords>::const_iterator i=bricks.begin();
       i!=bricks.end(); ++i)
    {
      coords n = pos + *i;
      cells.set(n, findex);
    }
  score += score_block();

  // search for filled lines
  filled = g->check_lines(cells, empty);
}

void game_board::next_level()
{
  // increment level
  ++level;
  switch (mode1)
    {
    case 0:
      // increase falling block speed
      speed = level;
      break;
    case 1:
      // increase maximum block size
      ++bmax;
      switch (mode2)
	{
	case 0: if (bmin > 1) --bmin; break;
	case 1: break;
	case 2: ++bmin; break;
	}
      start_blockset();
      break;
    case 2:
      // increase superblock size
      ++bsuper;
      start_blockset();
      break;
    }
}

void game_board::next_blockset()
{
  if (blocks.size() == 0)
    {
      blocks.push_back(g->block_zero());
      series.push_back(1);
      return;
    }

  // extract last blockset
  unsigned offset = blocks.size() - ((series.size() > 0) ? series.back() : 0);
  std::vector<unsigned> last(blocks.size() - offset);
  for (unsigned i=0; i<last.size(); ++i)
    last[i] = offset + i;

  // limit size in order to speed up generation
  unsigned max1 = g->blockset_max(), max2 = max1 * 5;
  if (max2 > last.size())
    max2 = last.size();

  if (max1 < last.size())
    // randomize
    for (unsigned i=0; i<max2; ++i)
      {
	unsigned index = i + (rand() % (last.size() - i));
	if (index > i)
	  std::swap(last[index], last[i]);
      }

  // generate and append new blockset
  std::vector<block> v;
  for (unsigned i=0; i<max2 && (i<max1 || !stop_blockset); ++i)
    g->augment_block(blocks[last[i]], v, cells.get_x0(), cells.get_z0(),
		     cells.get_cols(), cells.get_layers());

  // blocks, series may be reallocated
  // so this must be safeguarded to prevent the main thread from copying an
  // invalid block
  {
    // acquire lock
    std::lock_guard<std::mutex> lock(mutex);

    blocks.insert(blocks.end(), v.begin(), v.end());
    series.push_back(v.size());

    // lock released at end of scope
  }
}

void game_board::start_blockset()
{
  // stop worker if it hasn't already
  end_blockset();

  // offload blockset generation to a separate thread
  stop_blockset = false;
  worker = new std::thread(&game_board::next_blockset, this);
}

void game_board::end_blockset()
{
  // terminate blockset-generating thread
  stop_blockset = true;
  if (worker)
    {
      worker->join();
      delete worker;
      worker = 0;
    }
}

int game_board::score_block() const
{
  return (speed + bmax * 5 + bsuper) * (bmax + bmin - 5) / (1 + empty);
}

int game_board::score_lines(int lines) const
{
  return score_block() * 20 * (1 << lines);
}

game_board::~game_board()
{
  if (g)
    delete g;

  if (worker)
    {
      worker->join();
      delete worker;
      worker = 0;
    }
}

grid<bool> game_board::get_block() const
{
  grid<bool> g = fblock.shape[fshape];
  g.move(pos);
  return g;
}

grid<bool> game_board::get_shadow()
{
  // move block down until it lands
  coords c = pos;
  while (fits(fblock, fshape, g->down(c)))
    c = g->down(c);

  grid<bool> g = fblock.shape[fshape];
  g.move(c);
  return g;
}

void game_board::start_game(const group *gg, int width, int depth,
			    int m1, int m2, int s, int min, int max, int e)
{
  if (game)
    return;

  // delete previous group, if any
  if (g)
    delete g;

  // initialize random numbers generator
  Glib::TimeVal timeval;
  timeval.assign_current_time();
  std::srand(static_cast<unsigned>(timeval.as_double() * G_USEC_PER_SEC));

  // set spatial group
  g = gg;
  // generate game board
  cells = g->make_board(width, depth);
  // generate block set
  std::vector<block> v;
  blocks.resize(0);
  series.resize(0);
  stop_blockset = false;
  for (int i=0; i<=max; ++i)
    next_blockset();
  if (min > max)
    min = max;
  bmin = min;
  bmax = nsize = nsuper = max;
  isuper = bsuper = 0;
  empty = e;
  // start game
  mode1 = m1;
  if (mode1 == 2)
    {
      bsuper = max + 1;
      next_blockset();
    }
  mode2 = m2;
  speed = s;
  level = (mode1 == 0) ? speed : 1;
  lines = score = 0;
  filled.resize(0);
  game = true;

  // more blocksets needed
  if (mode1 == 1 || mode1 == 2)
    start_blockset();

  first_block();
}

bool game_board::rotate(int n)
{
  int rotations = fblock.rotate.size() / fblock.shape.size();
  if (n >= rotations)
    return false; // do nothing

  int nshape = fblock.rotate[fshape * rotations + n];
  coords m = move_in(fblock, nshape, pos);
  if (!fits(fblock, nshape, m))
    return false; // reject move

  // accept move
  pos = m;
  fshape = nshape;
  return true;
}

bool game_board::reflect()
{
  int nshape = fblock.reflect[fshape];
  coords m = move_in(fblock, nshape, pos);
  if (!fits(fblock, nshape, m))
    return false; // reject move

  // accept move
  pos = m;
  fshape = nshape;
  return true;
}

bool game_board::left()
{
  coords c = g->left(pos);
  if (!fits(fblock, fshape, c))
    return false; // reject move

  // accept move
  pos = c;
  return true;
}

bool game_board::right()
{
  coords c = g->right(pos);
  if (!fits(fblock, fshape, c))
    return false; // reject move

  // accept move
  pos = c;
  return true;
}

bool game_board::front()
{
  coords c = g->front(pos);
  if (!fits(fblock, fshape, c))
    return false; // reject move

  // accept move
  pos = c;
  return true;
}

bool game_board::back()
{
  coords c = g->back(pos);
  if (!fits(fblock, fshape, c))
    return false; // reject move

  // accept move
  pos = c;
  return true;
}

bool game_board::down()
{
  coords c = g->down(pos);
  if (fits(fblock, fshape, c) && fits_b(fblock, fshape, pos))
    {
      // move block down and continue
      pos = c;
      return true;
    }
  else
    {
      land_block();
      return false;
    }
}

bool game_board::drop()
{
  // move block down until it lands
  while (down())
    ;

  return true;
}

void game_board::color_lines(int c)
{
  for (std::vector<int>::const_iterator i=filled.begin();
       i!=filled.end(); ++i)
    g->color_line(cells, *i, c);
}

void game_board::remove_lines()
{
  // remove filled lines
  for (std::vector<int>::const_iterator i=filled.begin();
       i!=filled.end(); ++i)
    g->remove_line(cells, *i);

  // update lines count
  lines += filled.size();
  score += score_lines(filled.size());
  const int level_lines = 10;
  if (lines / level_lines > level)
    next_level();

  // choose new block
  next_block();
}

void game_board::next_block()
{
  // new active block
  fblock = nblock;
  findex = nindex;
  fshape = nshape;
  bsize = nsize;
  // set initial position
  pos = make_coords();
  if (findex != 0)
    pos = move_in(fblock, fshape, pos);
  // if it doesn't fit, game is over
  if (!fits(fblock, fshape, pos))
    {
      // stop worker if it hasn't already
      end_blockset();

      game = false;
    }

  // next block
  int max_super = bsuper;
  if (bsuper > 18)
    {
      max_super = (55 - bsuper) / 2;
      if (max_super < 2)
	max_super = 2;
    }
  if (mode1 == 2 && ++isuper >= max_super)
    {
      // superblock
      isuper = 0;
      nsuper = nsize;
      nsize = bsuper;
    }
  else
    {
      if (mode1 == 2 && isuper == 1)
	nsize = nsuper; // resume cycle where it was left

      // cycle over block sizes (not in order)
      int plus = (bmax - bmin + 1) / 2, minus = plus * 2 + 1;
      nsize += plus;
      while (nsize > bmax)
	nsize -= minus;
      if (nsize < bmin)
	nsize += (plus > 0) ? plus : 1;
    }
  // this must be safeguarded because blocks, series may be reallocated
  // in the worker thread
  {
    // acquire lock
    std::lock_guard<std::mutex> lock(mutex);

    // choose block
    nindex = rand() % series[nsize];
    for (int i=0; i<nsize; ++i)
      nindex += series[i];
    nblock = blocks[nindex];

    // lock released at end of scope
  }
  // choose orientation
  nshape = rand() % nblock.shape.size();
}

void game_board::stop()
{
  if (!game)
    return;

  // stop worker if it hasn't already
  end_blockset();

  game = false;
  cells = grid<int>(cells.get_rows(), cells.get_cols(), cells.get_layers(),
		    cells.get_c0());
}
