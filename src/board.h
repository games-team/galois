/* board.h -- game board for falling blocks game.  -*- C++ -*-
   Copyright (C) 2011-2020 Gerardo Ballabio

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#ifndef __BOARD_
#define __BOARD_

#include <mutex>
#include <thread>
#include <vector>
#include "grid.h"
#include "group.h"

class game_board
{
private:
  // game elements
  const group *g;
  grid<int> cells;
  std::vector<block> blocks;
  std::vector<unsigned> series; // how many blocks for each size

  // blockset generation
  std::thread *worker;
  std::mutex mutex;
  bool stop_blockset;

  // whether game is active
  bool game;

  // game options
  int mode1, mode2;
  int empty;

  // counters
  int bmin, bmax, bsuper, bsize, isuper, nsuper;
  int level, speed, lines, score;
  std::vector<int> filled;

  // falling block
  block fblock;
  int findex, fshape;
  coords pos;
  // next block
  block nblock;
  int nsize, nindex, nshape;

  bool fits(const block &b, int s, coords c) const;
  bool fits_b(const block &b, int s, coords c) const;
  coords move_in(const block &b, int s, coords c) const;

  void first_block();
  void land_block();
  void next_level();
  void next_blockset();

  void start_blockset();
  void end_blockset();

  int score_block() const;
  int score_lines(int lines) const;
public:
  game_board() : g(0), worker(0), game(false) { }
  ~game_board();

  // read-only access to internals
  const group *get_group() const { return g; }
  const grid<int> *get_cells() const { return &cells; }
  int get_index() const { return findex; }
  int get_next_index() const { return nindex; }
  grid<bool> get_block() const;
  const grid<bool> & get_next_block() const { return nblock.shape[nshape]; }
  bool is_game() const { return game; }
  int get_mode1() const { return mode1; }
  int get_bmin() const { return bmin; }
  int get_bmax() const { return bmax; }
  int get_bsuper() const { return bsuper; }
  int get_level() const { return level; }
  int get_speed() const { return speed; }
  int get_lines() const { return lines; }
  int get_score() const { return score; }
  const std::vector<int> get_filled() const { return filled; }

  grid<bool> get_shadow();

  // start new game
  void start_game(const group *gg, int w, int d, int mode1, int mode2,
		  int s, int min, int max, int e);

  // block moves
  bool rotate(int n);
  bool reflect();
  bool left();
  bool right();
  bool front();
  bool back();
  bool down();
  bool drop();

  // actions
  void color_lines(int c);
  void remove_lines();
  void next_block();
  void stop();
};

#endif /* __BOARD_ */
