/* cube.cc -- spatial groups for cubic bricks.  -*- C++ -*-
   Copyright (C) 2011-2020 Gerardo Ballabio

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#include <cmath>
#include <vector>
#include <gtkmm.h>
#include "grid.h"
#include "group.h"
#include "cube.h"

unsigned group_cube::rotate(unsigned n, unsigned m) const
{
  const unsigned r0[] = { 16,  8,  4, 12, 20, 13,  0, 11, 21,  5,  3, 19,
			  23, 17,  1,  7, 22,  9,  2, 15, 18, 14,  6, 10, };
  const unsigned r1[] = { 11,  7, 15, 19, 12,  3, 10, 23,  4,  2, 18, 20,
			  16,  0,  6, 22,  8,  1, 14, 21, 13,  5,  9, 17, };
  const unsigned r2[] = {  1,  2,  3,  0,  5,  6,  7,  4,  9, 10, 11,  8,
			   13, 14, 15, 12, 17, 18, 19, 16, 21, 22, 23, 20, };
  n = n % size();
  switch (m % 3)
    {
    case 0: return r0[n];
    case 1: return r1[n];
    case 2: return r2[n];
    default: return n; // suppress warning
    }
}

unsigned group_cube::reflect(unsigned n) const
{
  return n % size();
}

std::vector<coords> group_cube::neighbors(const coords &c) const
{
  std::vector<coords> v;
  v.push_back(c + make_coords(0, -1, 0));
  v.push_back(c + make_coords(-1, 0, 0));
  v.push_back(c + make_coords(0, 0, -1));
  v.push_back(c + make_coords(0, 0, 1));
  v.push_back(c + make_coords(1, 0, 0));
  v.push_back(c + make_coords(0, 1, 0));
  return v;
}

grid<bool> group_cube::transform(const grid<bool> &g, unsigned n) const
{
  // rotation matrix coefficients
  const int xx[] = {  1,  0, -1,  0,  0,  0,  0,  0,  0, -1,  0,  1,
		      0,  1,  0, -1,  0,  0,  0,  0,  1,  0, -1,  0, };
  const int xy[] = {  0, -1,  0,  1,  0,  0,  0,  0, -1,  0,  1,  0,
		      1,  0, -1,  0,  0,  0,  0,  0,  0, -1,  0,  1, };
  const int xz[] = {  0,  0,  0,  0, -1, -1, -1, -1,  0,  0,  0,  0,
		      0,  0,  0,  0,  1,  1,  1,  1,  0,  0,  0,  0, };
  const int yx[] = {  0,  1,  0, -1,  0, -1,  0,  1,  0,  0,  0,  0,
		      0,  0,  0,  0,  0,  1,  0, -1,  0, -1,  0,  1, };
  const int yy[] = {  1,  0, -1,  0, -1,  0,  1,  0,  0,  0,  0,  0,
		      0,  0,  0,  0,  1,  0, -1,  0, -1,  0,  1,  0, };
  const int yz[] = {  0,  0,  0,  0,  0,  0,  0,  0,  1,  1,  1,  1,
		     -1, -1, -1, -1,  0,  0,  0,  0,  0,  0,  0,  0, };
  const int zx[] = {  0,  0,  0,  0, -1,  0,  1,  0, -1,  0,  1,  0,
		     -1,  0,  1,  0, -1,  0,  1,  0,  0,  0,  0,  0, };
  const int zy[] = {  0,  0,  0,  0,  0,  1,  0, -1,  0,  1,  0, -1,
		      0,  1,  0, -1,  0,  1,  0, -1,  0,  0,  0,  0, };
  const int zz[] = {  1,  1,  1,  1,  0,  0,  0,  0,  0,  0,  0,  0,
		      0,  0,  0,  0,  0,  0,  0,  0, -1, -1, -1, -1, };

  n = n % size();
  grid<bool> t;
  for (int i=0; i<g.get_layers(); ++i)
    for (int j=0; j<g.get_rows(); ++j)
      for (int k=0; k<g.get_cols(); ++k)
	{
	  coords c = g.get_c0() + make_coords(k, j, i),
	    r = make_coords(xx[n] * c.x + yx[n] * c.y + zx[n] * c.z,
			    xy[n] * c.x + yy[n] * c.y + zy[n] * c.z,
			    xz[n] * c.x + yz[n] * c.y + zz[n] * c.z);
	  if (g.get(c))
	    t.add(r);
	}
  return t;
}

grid<bool> group_cube::center(const grid<bool> &g) const
{
  // find center of mass
  coords s = make_coords(), m = make_coords();
  int n = 0;
  for (int i=0; i<g.get_layers(); ++i)
    for (int j=0; j<g.get_rows(); ++j)
      for (int k=0; k<g.get_cols(); ++k)
	{
	  coords c = g.get_c0() + make_coords(k, j, i);
	  if (g.get(c))
	    {
	      s = s + c;
	      ++n;
	    }
	}
  if (n > 0)
    m = make_coords(-static_cast<int>(std::floor(double(s.x) / n + 0.5)),
		    -static_cast<int>(std::floor(double(s.y) / n + 0.5)),
		    -static_cast<int>(std::floor(double(s.z) / n + 0.5)));

  grid<bool> c = g;
  c.move(m);
  return c;
}

std::vector<int> group_cube::check_lines(grid<int> &g, int n) const
{
  std::vector<int> filled;
  for (int i=0; i<g.get_rows(); ++i)
    {
      // count empty cells
      int empty = 0;
      for (int j=0; j<g.get_layers(); ++j)
	for (int k=0; k<g.get_cols(); ++k)
	  {
	    coords c = g.get_c0() + make_coords(k, i, j);
	    if (!g.get(c))
	      ++empty;
	  }
      if (empty <= n)
	filled.push_back(i);
    }

  return filled;
}

void group_cube::color_line(grid<int> &g, int n, int color) const
{
  for (int i=0; i<g.get_layers(); ++i)
    for (int j=0; j<g.get_cols(); ++j)
      {
	coords c = make_coords(g.get_x0() + j, n, g.get_z0() + i);
	if (g.get(c))
	  g.set(c, -color);
      }
}

void group_cube::remove_line(grid<int> &g, int n) const
{
  for (int i=n; i>=0; --i)
    for (int j=0; j<g.get_layers(); ++j)
      for (int k=0; k<g.get_cols(); ++k)
	{
	  coords c = g.get_c0() + make_coords(k, i, j);
	  g.set(c, ((c.y > 0) ? g.get(c + make_coords(0, -1, 0)) : 0));
	}
}

Gdk::Rectangle group_cube::block_size(const grid<bool> &b, int size) const
{
  int offset1 = (size - 1) / 4, offset2 = (size - 1) / 3;
  return Gdk::Rectangle
    (b.get_x0() * size + b.get_y0() * offset1,
     (1 - b.get_layers() - b.get_z0()) * size + b.get_y0() * offset2,
     b.get_cols() * size + (b.get_rows() - 0) * offset1,
     b.get_layers() * size + (b.get_rows() - 0) * offset2);
}

Gdk::Rectangle group_cube::board_size(int size, int rows, int cols,
				      int layers) const
{
  int offset1 = (size - 1) / 4, offset2 = (size - 1) / 3;
  return Gdk::Rectangle(-(cols + 2) / 2 * size, -(layers - 1) / 2 * size,
			(cols + 2) * size + (rows - 1) * offset1,
			(layers + 1) * size + (rows - 1) * offset2);
}

void group_cube::draw_brick(Cairo::RefPtr<Cairo::Context> context,
			    const coords &c, const Gdk::RGBA &color,
			    int size) const
{
  const double lwidth = 2.0, margin = (lwidth + 1.0) * 0.5;
  context->set_line_width(lwidth);

  int offset1 = (size - 1) / 4, offset2 = (size - 1) / 3;
  context->move_to(c.x * size + c.y * offset1 + margin,
		   -c.z * size + c.y * offset2 + margin);
  context->line_to(c.x * size + c.y * offset1 + margin,
		   (1 - c.z) * size + c.y * offset2 - margin);
  context->line_to(c.x * size + (c.y + 1) * offset1 + margin - 1,
		   (1 - c.z) * size + (c.y + 1) * offset2 - margin - 1);
  context->line_to((c.x + 1) * size + (c.y + 1) * offset1 - margin - 1,
		   (1 - c.z) * size + (c.y + 1) * offset2 - margin - 1);
  context->line_to((c.x + 1) * size + (c.y + 1) * offset1 - margin - 1,
		   -c.z * size + (c.y + 1) * offset2 + margin - 1);
  context->line_to((c.x + 1) * size + c.y * offset1 - margin,
		   -c.z * size + c.y * offset2 + margin);
  context->close_path();
  context->set_source_rgba(color.get_red(), color.get_green(),
			   color.get_blue(), 0.8);
  context->fill();

  context->rectangle(c.x * size + c.y * offset1 + margin,
		     -c.z * size + c.y * offset2 + margin,
		     size - 2.0 * margin, size - 2.0 * margin);
  context->set_source_rgba(color.get_red(), color.get_green(),
			   color.get_blue(), 1.0);
  context->stroke();
}

void group_cube::draw_shadow(Cairo::RefPtr<Cairo::Context> context,
			     const coords &c, const Gdk::RGBA &color,
			     int size) const
{
  const double lwidth = 2.0, margin = (lwidth + 1.0) * 0.5;
  context->set_line_width(lwidth);

  int offset1 = (size - 1) / 4, offset2 = (size - 1) / 3;
  context->rectangle(c.x * size + (c.y + 1) * offset1 - margin,
		     -c.z * size + (c.y + 1) * offset2 - margin,
		     size - 2.0 * margin, size - 2.0 * margin);

  const double back = 0.8;
  context->set_source_rgba(color.get_red() * back, color.get_green() * back,
			   color.get_blue() * back, 1.0);
  context->stroke();

  context->rectangle(c.x * size + c.y * offset1 + margin,
		     -c.z * size + c.y * offset2 + margin,
		     size - 2.0 * margin, size - 2.0 * margin);

  const double front = 0.9;
  context->set_source_rgba(color.get_red() * front, color.get_green() * front,
			   color.get_blue() * front, 1.0);
  context->stroke();
}

void group_cube::draw_board(Cairo::RefPtr<Cairo::Context> context,
			    int rows, int cols, int layers, int size) const
{
  const double width = 2.0, margin = (width + 1.0) * 0.5;
  context->set_line_width(width);

  int offset1 = (size - 1) / 4, offset2 = (size - 1) / 3;
  int left = -cols / 2, right = left + cols, bottom = rows,
    front = -layers / 2, back = front + layers;

  // left side
  context->move_to(left * size - (layers * size + margin) *
		   offset1 / offset2, (1 - back) * size);
  // front side
  context->line_to(left * size + bottom * offset1,
		   (1 - front) * size + bottom * offset2 + margin);
  context->line_to(right * size + bottom * offset1 + margin,
		   (1 - front) * size + bottom * offset2 + margin);
  // right side
  context->line_to(right * size + bottom * offset1 + margin,
		   (1 - back) * size + bottom * offset2);
  // back side
  context->line_to(right * size + margin, (1 - back) * size);

  // clear inside
  context->set_source_rgba(0.0, 0.0, 0.0, 1.0);
  context->fill_preserve();

  // add other edges

  // base rectangle
  context->move_to(left * size + bottom * offset1,
		   (1 - front) * size + bottom * offset2 + margin);
  context->line_to(left * size + bottom * offset1,
		   (1 - back) * size + bottom * offset2);
  context->line_to(right * size + bottom * offset1 + margin,
		   (1 - back) * size + bottom * offset2);
  // side edge
  context->move_to(left * size, (1 - back) * size);
  context->line_to(left * size + bottom * offset1,
		   (1 - back) * size + bottom * offset2);

  // draw outline
  context->set_source_rgba(1.0, 1.0, 1.0, 1.0);
  context->stroke();
}

void group_cube::draw_board_front(Cairo::RefPtr<Cairo::Context> context,
				  int rows, int cols, int layers,
				  int size) const
{
  const double width = 2.0, margin = (width + 1.0) * 0.5;
  context->set_line_width(width);

  int offset1 = (size - 1) / 4, offset2 = (size - 1) / 3;
  int left = -cols / 2, right = left + cols, bottom = rows,
    front = -layers / 2, back = front + layers;

  // front edge
  context->move_to(right * size + margin - (layers * size + margin) *
		   offset1 / offset2, (1 - back) * size);
  context->line_to(right * size + bottom * offset1 + margin,
		   (1 - front) * size + bottom * offset2 + margin);

  context->set_source_rgba(1.0, 1.0, 1.0, 1.0);
  context->stroke();
}

void group_cube::draw_plane(Cairo::RefPtr<Cairo::Context> context, int y,
			    int cols, int layers, int size) const
{
  const double width = 2.0, margin = (width + 1.0) * 0.5;
  context->set_line_width(width);

  int offset1 = (size - 1) / 4, offset2 = (size - 1) / 3;
  int left = -cols / 2, front = -layers / 2, back = front + layers;

  context->rectangle(left * size + y * offset1 + offset1 / 2,
		     (1 - back) * size + y * offset2 + offset2 / 2,
		     cols * size + margin, layers * size + margin);
  context->set_source_rgba(1.0, 1.0, 1.0, 1.0);
  context->stroke();
}

unsigned group_cube_mirror::rotate(unsigned n, unsigned m) const
{
  const unsigned r0[] = { 16,  8,  4, 12, 20, 13,  0, 11, 21,  5,  3, 19,
			  23, 17,  1,  7, 22,  9,  2, 15, 18, 14,  6, 10,
			  30, 38, 42, 34, 26, 33, 46, 39, 25, 41, 47, 31,
			  27, 29, 45, 43, 24, 37, 44, 35, 28, 32, 40, 36, };
  const unsigned r1[] = { 11,  7, 15, 19, 12,  3, 10, 23,  4,  2, 18, 20,
			  16,  0,  6, 22,  8,  1, 14, 21, 13,  5,  9, 17,
			  35, 31, 39, 43, 36, 27, 34, 47, 28, 26, 42, 44,
			  40, 24, 30, 46, 32, 25, 38, 45, 37, 29, 33, 41, };
  const unsigned r2[] = {  1,  2,  3,  0,  5,  6,  7,  4,  9, 10, 11,  8,
			   13, 14, 15, 12, 17, 18, 19, 16, 21, 22, 23, 20,
			   27, 24, 25, 26, 31, 28, 29, 30, 35, 32, 33, 34,
			   39, 36, 37, 38, 43, 40, 41, 42, 47, 44, 45, 46, };
  n = n % size();
  switch (m % 3)
    {
    case 0: return r0[n];
    case 1: return r1[n];
    case 2: return r2[n];
    default: return n; // suppress warning
    }
}

unsigned group_cube_mirror::reflect(unsigned n) const
{
  const unsigned r[] = { 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35,
			 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47,
			 0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11,
			 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, };
  return r[n % size()];
}

grid<bool> group_cube_mirror::transform(const grid<bool> &g, unsigned n) const
{
  // rotation matrix coefficients
  const int xx[] = {  1,  0, -1,  0,  0,  0,  0,  0,  0, -1,  0,  1,
		      0,  1,  0, -1,  0,  0,  0,  0,  1,  0, -1,  0,
		     -1,  0,  1,  0,  0,  0,  0,  0,  0,  1,  0, -1,
		      0, -1,  0,  1,  0,  0,  0,  0, -1,  0,  1,  0, };
  const int xy[] = {  0, -1,  0,  1,  0,  0,  0,  0, -1,  0,  1,  0,
		      1,  0, -1,  0,  0,  0,  0,  0,  0, -1,  0,  1,
		      0, -1,  0,  1,  0,  0,  0,  0, -1,  0,  1,  0,
		      1,  0, -1,  0,  0,  0,  0,  0,  0, -1,  0,  1, };
  const int xz[] = {  0,  0,  0,  0, -1, -1, -1, -1,  0,  0,  0,  0,
		      0,  0,  0,  0,  1,  1,  1,  1,  0,  0,  0,  0,
		      0,  0,  0,  0, -1, -1, -1, -1,  0,  0,  0,  0,
		      0,  0,  0,  0,  1,  1,  1,  1,  0,  0,  0,  0, };
  const int yx[] = {  0,  1,  0, -1,  0, -1,  0,  1,  0,  0,  0,  0,
		      0,  0,  0,  0,  0,  1,  0, -1,  0, -1,  0,  1,
		      0, -1,  0,  1,  0,  1,  0, -1,  0,  0,  0,  0,
		      0,  0,  0,  0,  0, -1,  0,  1,  0,  1,  0, -1, };
  const int yy[] = {  1,  0, -1,  0, -1,  0,  1,  0,  0,  0,  0,  0,
		      0,  0,  0,  0,  1,  0, -1,  0, -1,  0,  1,  0,
		      1,  0, -1,  0, -1,  0,  1,  0,  0,  0,  0,  0,
		      0,  0,  0,  0,  1,  0, -1,  0, -1,  0,  1,  0, };
  const int yz[] = {  0,  0,  0,  0,  0,  0,  0,  0,  1,  1,  1,  1,
		     -1, -1, -1, -1,  0,  0,  0,  0,  0,  0,  0,  0,
		      0,  0,  0,  0,  0,  0,  0,  0,  1,  1,  1,  1,
		     -1, -1, -1, -1,  0,  0,  0,  0,  0,  0,  0,  0, };
  const int zx[] = {  0,  0,  0,  0, -1,  0,  1,  0, -1,  0,  1,  0,
		     -1,  0,  1,  0, -1,  0,  1,  0,  0,  0,  0,  0,
		      0,  0,  0,  0,  1,  0, -1,  0,  1,  0, -1,  0,
		      1,  0, -1,  0,  1,  0, -1,  0,  0,  0,  0,  0, };
  const int zy[] = {  0,  0,  0,  0,  0,  1,  0, -1,  0,  1,  0, -1,
		      0,  1,  0, -1,  0,  1,  0, -1,  0,  0,  0,  0,
		      0,  0,  0,  0,  0,  1,  0, -1,  0,  1,  0, -1,
		      0,  1,  0, -1,  0,  1,  0, -1,  0,  0,  0,  0, };
  const int zz[] = {  1,  1,  1,  1,  0,  0,  0,  0,  0,  0,  0,  0,
		      0,  0,  0,  0,  0,  0,  0,  0, -1, -1, -1, -1,
		      1,  1,  1,  1,  0,  0,  0,  0,  0,  0,  0,  0,
		      0,  0,  0,  0,  0,  0,  0,  0, -1, -1, -1, -1, };

  n = n % size();
  grid<bool> t;
  for (int i=0; i<g.get_layers(); ++i)
    for (int j=0; j<g.get_rows(); ++j)
      for (int k=0; k<g.get_cols(); ++k)
	{
	  coords c = g.get_c0() + make_coords(k, j, i),
	    r = make_coords(xx[n] * c.x + yx[n] * c.y + zx[n] * c.z,
			    xy[n] * c.x + yy[n] * c.y + zy[n] * c.z,
			    xz[n] * c.x + yz[n] * c.y + zz[n] * c.z);
	  if (g.get(c))
	    t.add(r);
	}
  return t;
}
