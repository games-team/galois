/* cube.h -- spatial groups for cubic bricks.  -*- C++ -*-
   Copyright (C) 2011-2020 Gerardo Ballabio

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#ifndef __CUBE_
#define __CUBE_

#include <vector>
#include <gtkmm.h>
#include "grid.h"
#include "group.h"

class group_cube : public group_3d
{
private:
  virtual unsigned size() const { return 24; }
  virtual unsigned rotate(unsigned n, unsigned m) const;
  virtual unsigned reflect(unsigned n) const;
  virtual int type(const coords &c) const { return 0; }
  virtual std::vector<coords> neighbors(const coords &c) const;
  virtual std::vector<coords> blockers(const coords &c) const
  { return std::vector<coords>(); }
  virtual grid<bool> transform(const grid<bool> &g, unsigned n) const;
  virtual grid<bool> center(const grid<bool> &g) const;
public:
  virtual Glib::ustring name() const { return "cube"; }
  virtual unsigned blockset_max() const { return 15; }

  // game
  virtual coords left(const coords &c) const
  { return c + make_coords(-1, 0, 0); }
  virtual coords right(const coords &c) const
  { return c + make_coords(1, 0, 0); }
  virtual coords front(const coords &c) const
  { return c + make_coords(0, 0, -1); }
  virtual coords back(const coords &c) const
  { return c + make_coords(0, 0, 1); }
  virtual coords down(const coords &c) const
  { return c + make_coords(0, 1, 0); }
  virtual grid<int> make_board(int width, int depth = 1) const
  { return grid<int>(30, width, depth, -width / 2, 0, -depth / 2); }
  virtual std::vector<int> check_lines(grid<int> &g, int n) const;
  virtual void color_line(grid<int> &g, int n, int c) const;
  virtual void remove_line(grid<int> &g, int n) const;

  // drawing methods
  virtual Gdk::Rectangle block_size(const grid<bool> &b, int size) const;
  virtual Gdk::Rectangle board_size(int size, int rows, int cols,
				    int layers) const;
  virtual void draw_brick(Cairo::RefPtr<Cairo::Context> context,
			  const coords &c, const Gdk::RGBA &color,
			  int size) const;
  virtual void draw_shadow(Cairo::RefPtr<Cairo::Context> context,
			   const coords &c, const Gdk::RGBA &color,
			   int size) const;
  virtual void draw_board(Cairo::RefPtr<Cairo::Context> context,
			  int rows, int cols, int layers, int size) const;
  virtual void draw_board_front(Cairo::RefPtr<Cairo::Context> context,
				int rows, int cols, int layers,
				int size) const;
  virtual void draw_plane(Cairo::RefPtr<Cairo::Context> context,
			  int y, int cols, int layers, int size) const;
};

class group_cube_mirror : public group_cube
{
private:
  virtual unsigned size() const { return 48; }
  virtual unsigned rotate(unsigned n, unsigned m) const;
  virtual unsigned reflect(unsigned n) const;
  virtual grid<bool> transform(const grid<bool> &g, unsigned n) const;
public:
  virtual Glib::ustring name() const { return "cube_m"; }
  virtual unsigned blockset_max() const { return 10; }
};

#endif /* __CUBE_ */
