/* grid.h -- three-dimensional grid of points.  -*- C++ -*-
   Copyright (C) 2011-2020 Gerardo Ballabio

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#ifndef __GRID_
#define __GRID_

#include <valarray>
#include <vector>
#include <gtkmm.h>

struct coords
{
  int x, y, z;
};

inline coords make_coords(int x = 0, int y = 0, int z = 0)
{
  coords c;
  c.x = x;
  c.y = y;
  c.z = z;
  return c;
}

inline coords operator-(const coords &c)
{
  return make_coords(-c.x, -c.y, -c.z);
}

inline coords operator+(const coords &c1, const coords &c2)
{
  return make_coords(c1.x + c2.x, c1.y + c2.y, c1.z + c2.z);
}

// required forward declarations
template <class T> class grid;
template <class T> bool operator==(const grid<T> &g1, const grid<T> &g2);

template <class T> class grid
{
private:
  std::valarray<T> mask;
  int rows, cols, layers; // dimensions
  coords c0; // front upper left corner (y goes downwards, z goes inwards)
public:
  grid() : rows(0), cols(0), layers(0), c0() { }
  grid(int r, int c, int l, int x = 0, int y = 0, int z = 0)
    : mask(T(0), r * c * l), rows(r), cols(c), layers(l),
      c0(make_coords(x, y, z)) { }
  grid(int r, int c, int l, const coords &cc)
    : mask(T(0), r * c * l), rows(r), cols(c), layers(l), c0(cc) { }

  // copy constructor and assignment
  grid(const grid &g)
    : mask(T(0), g.rows * g.cols * g.layers), rows(g.rows), cols(g.cols),
      layers(g.layers), c0(g.c0) { mask = g.mask; }
  grid & operator=(const grid &g)
  {
    rows = g.rows; cols = g.cols; layers = g.layers; c0 = g.c0;
    mask.resize(rows * cols * layers); mask = g.mask;
    return *this;
  }

  // simple operations
  int get_rows() const { return rows; }
  int get_cols() const { return cols; }
  int get_layers() const { return layers; }
  const coords & get_c0() const { return c0; }
  int get_x0() const { return c0.x; }
  int get_y0() const { return c0.y; }
  int get_z0() const { return c0.z; }
  T get(const coords &c) const
  { return mask[(c.x - c0.x) + cols * ((c.y - c0.y) + rows * (c.z - c0.z))]; }
  void set(const coords &c, T v = T(1))
  { mask[(c.x - c0.x) + cols * ((c.y - c0.y) + rows * (c.z - c0.z))] = v; }
  void clear(const coords &c) { set(c, T(0)); }

  // check bounds
  bool is_in(const coords &c) const;

  void move(const coords &c) { c0 = c0 + c; }
  void add(const coords &c, T v = T(1)); // this might need resizing

  friend bool operator==<>(const grid &g1, const grid &g2);
};

template <class T> bool grid<T>::is_in(const coords &c) const
{
  return (c.x >= c0.x && c.x < c0.x + cols &&
	  c.y >= c0.y && c.y < c0.y + rows &&
	  c.z >= c0.z && c.z < c0.z + layers);
}

template <class T> void grid<T>::add(const coords &c, T v)
{
  // resize if needed
  if (mask.size() == 0)
    {
      mask.resize(1);
      c0 = c;
      rows = cols = layers = 1;
    }
  else if (!is_in(c))
    {
      coords d0 = make_coords((c.x < c0.x) ? c0.x - c.x : 0,
			      (c.y < c0.y) ? c0.y - c.y : 0,
			      (c.z < c0.z) ? c0.z - c.z : 0),
	n0 = make_coords(c0.x - d0.x, c0.y - d0.y, c0.z - d0.z);
      int dcols = (c.x >= c0.x + cols) ? c.x - c0.x - cols + 1 : 0,
	drows = (c.y >= c0.y + rows) ? c.y - c0.y - rows + 1 : 0,
	dlayers = (c.z >= c0.z + layers) ? c.z - c0.z - layers + 1 : 0;
      int nrows = rows + d0.y + drows, ncols = cols + d0.x + dcols,
	nlayers = layers + d0.z + dlayers;

      // resize, copy old values
      std::valarray<T> m = mask;
      mask.resize(nrows * ncols * nlayers, T(0));
      size_t len[] = { size_t(layers), size_t(rows), size_t(cols), };
      size_t str[] = { size_t(ncols * nrows), size_t(ncols), 1, };
      std::gslice g(d0.x + ncols * (d0.y + nrows * d0.z),
		    std::valarray<size_t>(len, 3),
		    std::valarray<size_t>(str, 3));
      mask[g] = m;

      c0 = n0;
      rows = nrows;
      cols = ncols;
      layers = nlayers;
    }

  // set value
  set(c, v);
}

template <class T> bool operator==(const grid<T> &g1, const grid<T> &g2)
{
  // check dimensions
  if (g1.rows != g2.rows || g1.cols != g2.cols || g1.layers != g2.layers ||
      g1.c0.x != g2.c0.x || g1.c0.y != g2.c0.y || g1.c0.z != g2.c0.z)
    return false;

  // check contents
  for (unsigned i=0; i<g1.mask.size(); ++i)
    if (g1.mask[i] != g2.mask[i])
      return false;

  // no difference found
  return true;
}

inline std::vector<coords> get_bricks(const grid<bool> &g)
{
  std::vector<coords> v;
  for (int i=0; i<g.get_layers(); ++i)
    for (int j=0; j<g.get_rows(); ++j)
      for (int k=0; k<g.get_cols(); ++k)
	{
	  coords c = g.get_c0() + make_coords(k, j, i);
	  if (g.get(c))
	    v.push_back(c);
	}
  return v;
}

#endif /* __GRID_ */
