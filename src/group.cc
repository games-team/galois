/* group.cc -- spatial groups for falling block game.  -*- C++ -*-
   Copyright (C) 2011-2020 Gerardo Ballabio

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#include <vector>
#include <gtkmm.h>
#include "grid.h"
#include "group.h"

grid<bool> group::augmentation_set(const grid<bool> &g) const
{
  grid<bool> s;

  if (g.get_rows() == 0 || g.get_cols() == 0 || g.get_layers() == 0)
    {
      s.add(make_coords());
      return s;
    }

  for (int i=0; i<g.get_layers(); ++i)
    for (int j=0; j<g.get_rows(); ++j)
      for (int k=0; k<g.get_cols(); ++k)
	{
	  coords c = g.get_c0() + make_coords(k, j, i);
	  if (g.get(c))
	    {
	      std::vector<coords> n = neighbors(c);
	      for (std::vector<coords>::const_iterator l=n.begin();
		   l!=n.end(); ++l)
		if (!g.is_in(*l) || !g.get(*l))
		  s.add(*l);
	    }
	}
  return s;
}

std::vector<grid<bool> > group::augment_grid(const grid<bool> &g) const
{
  std::vector<grid<bool> > v;
  grid<bool> s = augmentation_set(g);
  for (int i=0; i<s.get_layers(); ++i)
    for (int j=0; j<s.get_rows(); ++j)
      for (int k=0; k<s.get_cols(); ++k)
	{
	  coords c = s.get_c0() + make_coords(k, j, i);
	  if (s.get(c))
	    {
	      grid<bool> t = g;
	      t.add(c);
	      v.push_back(center(t));
	    }
	}
  return v;
}

grid<bool> group::blockers_set(const grid<bool> &g) const
{
  grid<bool> s;

  if (g.get_rows() == 0 || g.get_cols() == 0 || g.get_layers() == 0)
    {
      s.add(make_coords());
      return s;
    }

  for (int i=0; i<g.get_layers(); ++i)
    for (int j=0; j<g.get_rows(); ++j)
      for (int k=0; k<g.get_cols(); ++k)
	{
	  coords c = g.get_c0() + make_coords(k, j, i);
	  if (g.get(c))
	    {
	      std::vector<coords> b = blockers(c);
	      for (std::vector<coords>::const_iterator l=b.begin();
		   l!=b.end(); ++l)
		if (!g.is_in(*l) || !g.get(*l))
		  s.add(*l);
	    }
	}
  return s;
}

block group::make_block(const grid<bool> &g) const
{
  block b;

  // find symmetry subgroup for this block
  std::vector<unsigned> subgroup;
  for (unsigned i=0; i<size(); ++i)
    {
      grid<bool> t = center(transform(g, i));
      bool dup = false;
      for (std::vector<grid<bool> >::const_iterator j=b.shape.begin();
	   j!=b.shape.end(); ++j)
	if (t == *j)
	  {
	    dup = true;
	    subgroup.push_back(j - b.shape.begin());
	    break;
	  }
      if (!dup)
	{
	  b.shape.push_back(t);
	  b.blocker.push_back(blockers_set(t));
	  subgroup.push_back(b.shape.size() - 1);
	}
    }

  // remap rotate and reflect onto subgroup
  for (std::vector<unsigned>::const_iterator i=subgroup.begin();
       i!=subgroup.end(); ++i)
    if (*i == b.reflect.size())
      {
	for (unsigned j=0; j<rotations(); ++j)
	  b.rotate.push_back(subgroup[rotate(i - subgroup.begin(), j)]);
	b.reflect.push_back(subgroup[reflect(i - subgroup.begin())]);
      }

  return b;
}

block group::block_zero() const
{
  block b;
  b.shape.push_back(grid<bool>());
  b.blocker.push_back(grid<bool>());
  for (unsigned i=0; i<rotations(); ++i)
    b.rotate.push_back(0);
  b.reflect.push_back(0);
  return b;
}

void group::augment_block(const block &b, std::vector<block> &v,
			  int x0, int z0, int cols, int layers) const
{
  std::vector<grid<bool> > a = augment_grid(b.shape.front());
  for (std::vector<grid<bool> >::const_iterator i=a.begin();
       i!=a.end(); ++i)
    {
      bool dup = false;
      for (std::vector<block>::const_iterator j=v.begin();
	   j!=v.end(); ++j)
	for (std::vector<grid<bool> >::const_iterator k=j->shape.begin();
	     k!=j->shape.end(); ++k)
	  if (*i == *k)
	    {
	      dup = true;
	      break;
	    }
      if (!dup)
	{
	  block b = make_block(*i);
	  bool fits = true;
	  if (cols > 1)
	    {
	      // check whether block fits within bounds
	      for (std::vector<grid<bool> >::const_iterator l=b.shape.begin();
		   l!=b.shape.end(); ++l)
		if (l->get_cols() > cols || l->get_layers() > layers)
		  fits = false;
	    }
	  if (fits)
	    v.push_back(b);
	}
    }
}
