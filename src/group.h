/* group.h -- spatial groups for falling block game.  -*- C++ -*-
   Copyright (C) 2011-2020 Gerardo Ballabio

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#ifndef __GROUP_
#define __GROUP_

#include <vector>
#include <gtkmm.h>
#include "grid.h"

struct block
{
  std::vector<grid<bool> > shape, blocker;
  std::vector<unsigned> rotate, reflect;
};

// virtual base class
class group
{
  // abstract interface
private:
  // define symmetry group, translations etc.
  virtual unsigned size() const = 0;
  virtual unsigned rotations() const = 0;
  virtual unsigned rotate(unsigned n, unsigned m) const = 0;
  virtual unsigned reflect(unsigned n) const = 0;
  virtual int type(const coords &c) const = 0;
  virtual std::vector<coords> neighbors(const coords &c) const = 0;
  virtual std::vector<coords> blockers(const coords &c) const = 0;
  virtual grid<bool> transform(const grid<bool> &g, unsigned n) const = 0;
  virtual grid<bool> center(const grid<bool> &g) const = 0;
public:
  virtual bool is_3d() const = 0;
  virtual unsigned blockset_max() const = 0; // values determined empirically

  // game
  virtual coords left(const coords &c) const = 0;
  virtual coords right(const coords &c) const = 0;
  virtual coords front(const coords &c) const = 0;
  virtual coords back(const coords &c) const = 0;
  virtual coords down(const coords &c) const = 0;
  virtual grid<int> make_board(int width, int depth = 1) const = 0;
  virtual std::vector<int> check_lines(grid<int> &g, int n) const = 0;
  virtual void color_line(grid<int> &g, int n, int color) const = 0;
  virtual void remove_line(grid<int> &g, int n) const = 0;

  // drawing methods
  virtual Gdk::Rectangle block_size(const grid<bool> &b, int size) const = 0;
  virtual Gdk::Rectangle board_size(int size, int rows, int cols,
				    int layers) const = 0;
  virtual void draw_brick(Cairo::RefPtr<Cairo::Context> context,
			  const coords &c, const Gdk::RGBA &color,
			  int size) const = 0;
  virtual void draw_shadow(Cairo::RefPtr<Cairo::Context> context,
			   const coords &c, const Gdk::RGBA &color,
			   int size) const = 0;
  virtual void draw_board(Cairo::RefPtr<Cairo::Context> context,
			  int rows, int cols, int layers, int size) const = 0;

  virtual ~group() { }

  // generic methods
private:
  grid<bool> augmentation_set(const grid<bool> &g) const;
  std::vector<grid<bool> > augment_grid(const grid<bool> &g) const;
  grid<bool> blockers_set(const grid<bool> &g) const;
  block make_block(const grid<bool> &g) const;
public:
  block block_zero() const;
  void augment_block(const block &b, std::vector<block> &v,
		     int x0 = 0, int z0 = 0,
		     int cols = 1, int layers = 1) const;
};

class group_2d : public group
{
private:
  virtual unsigned rotations() const { return 1; }
  virtual coords front(const coords &c) const { return c; }
  virtual coords back(const coords &c) const { return c; }
public:
  virtual bool is_3d() const { return false; }
};

class group_3d : public group
{
private:
  virtual unsigned rotations() const { return 3; }
public:
  virtual bool is_3d() const { return true; }
  virtual void draw_board_front(Cairo::RefPtr<Cairo::Context> context,
				int rows, int cols, int layers,
				int size) const = 0;
  virtual void draw_plane(Cairo::RefPtr<Cairo::Context> context,
			  int y, int cols, int layers, int size) const = 0;
};

#endif /* __GROUP_ */
