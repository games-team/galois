/* hexagon.cc -- spatial groups for hexagonal bricks.  -*- C++ -*-
   Copyright (C) 2011-2020 Gerardo Ballabio

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#include <cmath>
#include <vector>
#include <gtkmm.h>
#include "grid.h"
#include "group.h"
#include "hexagon.h"

unsigned group_hexagon::rotate(unsigned n, unsigned m) const
{
  const unsigned r[] = { 1, 2, 3, 4, 5, 0, };
  return r[n % size()];
}

unsigned group_hexagon::reflect(unsigned n) const
{
  return n % size();
}

std::vector<coords> group_hexagon::neighbors(const coords &c) const
{
  std::vector<coords> v;
  v.push_back(c + make_coords(0, -2));
  v.push_back(c + make_coords(-1, -1));
  v.push_back(c + make_coords(1, -1));
  v.push_back(c + make_coords(-1, 1));
  v.push_back(c + make_coords(1, 1));
  v.push_back(c + make_coords(0, 2));
  return v;
}

grid<bool> group_hexagon::transform(const grid<bool> &g, unsigned n) const
{
  // rotation matrix coefficients (times 2)
  const int xx[] = { 2,  1, -1, -2, -1,  1, };
  const int xy[] = { 0, -3, -3,  0,  3,  3, };
  const int yx[] = { 0,  1,  1,  0, -1, -1, };
  const int yy[] = { 2,  1, -1, -2, -1,  1, };

  n = n % size();
  grid<bool> t;
  for (int i=0; i<g.get_rows(); ++i)
    for (int j=0; j<g.get_cols(); ++j)
      {
	coords c = g.get_c0() + make_coords(j, i),
	  r = make_coords((xx[n] * c.x + yx[n] * c.y) / 2,
			  (xy[n] * c.x + yy[n] * c.y) / 2);
	if (g.get(c))
	  t.add(r);
      }
  return t;
}

grid<bool> group_hexagon::center(const grid<bool> &g) const
{
  // find center of mass
  coords s = make_coords(), m = make_coords();
  int n = 0;
  for (int i=0; i<g.get_rows(); ++i)
    for (int j=0; j<g.get_cols(); ++j)
      {
	coords c = g.get_c0() + make_coords(j, i);
	// change coordinate system
	coords t = make_coords((c.x + c.y) / 2, (c.y - c.x) / 2);
	if (g.get(c))
	  {
	    s = s + t;
	    ++n;
	  }
      }
  if (n > 0)
    m = make_coords(-static_cast<int>(std::floor(double(s.x) / n + 0.5)),
		    -static_cast<int>(std::floor(double(s.y) / n + 0.5)));
  // change back coordinate system
  coords mm = make_coords(m.x - m.y, m.x + m.y);

  grid<bool> c = g;
  c.move(mm);
  return c;
}

std::vector<int> group_hexagon::check_lines(grid<int> &g, int n) const
{
  std::vector<int> filled;
  for (int i=1; i<g.get_rows(); ++i)
    {
      // count empty cells
      int empty = 0;
      for (int j=0; j<g.get_cols(); ++j)
	{
	  coords c = g.get_c0() + make_coords(j, i);
	  if (type(c) < 0)
	    c = c + make_coords(0, -1);
	  if (!g.get(c))
	    ++empty;
	}
      if (empty <= n)
	{
	  filled.push_back(i);
	  ++i; // skip next line
	}
    }

  return filled;
}

void group_hexagon::color_line(grid<int> &g, int n, int color) const
{
  for (int i=n-1; i<=n; ++i)
    for (int j=0; j<g.get_cols(); ++j)
      {
	coords c = g.get_c0() + make_coords(j, i);
	if (type(c) >= 0 && g.get(c))
	  g.set(c, color);
      }
}

void group_hexagon::remove_line(grid<int> &g, int n) const
{
  for (int i=n; i>=0; --i)
    for (int j=0; j<g.get_cols(); ++j)
      {
	coords c = g.get_c0() + make_coords(j, i);
	if (type(c) == 0)
	  g.set(c, ((c.y > 1) ? g.get(c + make_coords(0, -2)) : 0));
      }
}

Gdk::Rectangle group_hexagon::block_size(const grid<bool> &b, int size) const
{
  double xunit = size * 0.288675134594813, yunit = size * 0.5;
  return Gdk::Rectangle(b.get_x0() * 3 * xunit, b.get_y0() * yunit,
			(b.get_cols() * 3 + 1) * xunit,
			(b.get_rows() + 1) * yunit);
}

Gdk::Rectangle group_hexagon::board_size(int size, int rows, int cols,
					 int layers) const
{
  double xunit = size * 0.288675134594813, yunit = size * 0.5;
  return Gdk::Rectangle
    (-static_cast<int>(std::ceil((cols + 2) / 2 * 3 * xunit)), 0,
     static_cast<int>(std::ceil(((cols + 2) * 3 + 1) * xunit)),
     static_cast<int>(std::ceil((rows + 2) * yunit)));
}

void group_hexagon::draw_brick(Cairo::RefPtr<Cairo::Context> context,
			       const coords &c, const Gdk::RGBA &color,
			       int size) const
{
  if (type(c) < 0)
    return;

  const double lwidth = 2.0, margin = (lwidth + 1.0) * 0.5;
  context->set_line_width(lwidth);

  double xunit = size * 0.288675134594813, yunit = size * 0.5;
  double xmargin = margin * 0.577350269189626, ymargin = margin;
  context->move_to((3 * c.x + 4) * xunit - 2 * xmargin, (c.y + 1) * yunit);
  context->line_to((3 * c.x + 3) * xunit - xmargin, c.y * yunit + ymargin);
  context->line_to((3 * c.x + 1) * xunit + xmargin, c.y * yunit + ymargin);
  context->line_to(3 * c.x * xunit + 2 * xmargin, (c.y + 1) * yunit);
  context->line_to((3 * c.x + 1) * xunit + xmargin,
		   (c.y + 2) * yunit - ymargin);
  context->line_to((3 * c.x + 3) * xunit - xmargin,
		   (c.y + 2) * yunit - ymargin);
  context->close_path();

  context->set_source_rgba(color.get_red(), color.get_green(),
			   color.get_blue(), 0.8);
  context->fill_preserve();
  context->set_source_rgba(color.get_red(), color.get_green(),
			   color.get_blue(), 1.0);
  context->stroke();
}

void group_hexagon::draw_shadow(Cairo::RefPtr<Cairo::Context> context,
				const coords &c, const Gdk::RGBA &color,
				int size) const
{
  if (type(c) < 0)
    return;

  const double lwidth = 2.0, margin = (lwidth + 1.0) * 0.5;
  context->set_line_width(lwidth);

  double xunit = size * 0.288675134594813, yunit = size * 0.5;
  double xmargin = margin * 0.577350269189626, ymargin = margin;
  context->move_to((3 * c.x + 4) * xunit - 2 * xmargin, (c.y + 1) * yunit);
  context->line_to((3 * c.x + 3) * xunit - xmargin, c.y * yunit + ymargin);
  context->line_to((3 * c.x + 1) * xunit + xmargin, c.y * yunit + ymargin);
  context->line_to(3 * c.x * xunit + 2 * xmargin, (c.y + 1) * yunit);
  context->line_to((3 * c.x + 1) * xunit + xmargin,
		   (c.y + 2) * yunit - ymargin);
  context->line_to((3 * c.x + 3) * xunit - xmargin,
		   (c.y + 2) * yunit - ymargin);
  context->close_path();

  const double dark = 0.8;
  context->set_source_rgba(color.get_red() * dark, color.get_green() * dark,
			   color.get_blue() * dark, 1.0);
  context->stroke();
}

void group_hexagon::draw_board(Cairo::RefPtr<Cairo::Context> context,
			       int rows, int cols, int layers, int size) const
{
  const double width = 2.0, margin = (width + 1.0) * 0.5;
  context->set_line_width(width);

  double xunit = size * 0.288675134594813, yunit = size * 0.5;
  double xmargin = margin * 0.577350269189626, ymargin = margin;

  int left = -cols / 2 - 1, right = left + cols + 1, bottom = rows;
  int lparity = ((left + bottom) % 2 == 0) ? 0 : 1,
    rparity = ((right + bottom) % 2 == 0) ? 0 : 1;

  // left side
  for (int i=0; i<bottom+1-lparity; ++i)
    {
      int iparity = ((i + bottom + lparity) % 2 == 0) ? 0 : 1;
      context->line_to(((left + 1) * 3 + iparity) * xunit - 2.0 * xmargin,
		       i * yunit);
    }
  // bottom left corner
  context->line_to((left * 3 + 4) * xunit - xmargin,
		   (bottom + 1 - lparity) * yunit + ymargin);
  // bottom
  for (int i=left+1; i<right; ++i)
    {
      int iparity = ((i + bottom) % 2 == 0) ? 0 : 1,
	isign = (iparity == 0) ? 1 : -1;
      context->line_to((i * 3 + 1) * xunit + isign * xmargin,
		       (bottom + iparity) * yunit + ymargin);
      context->line_to((i * 3 + 3) * xunit - isign * xmargin,
		       (bottom + iparity) * yunit + ymargin);
    }
  // bottom right corner
  context->line_to(right * 3 * xunit + xmargin,
		   (bottom + 1 - rparity) * yunit + ymargin);
  // right side
  for (int i=bottom-rparity; i>=0; --i)
    {
      int iparity = ((i + bottom + rparity + 1) % 2 == 0) ? 0 : 1;
      context->line_to((right * 3 + iparity) * xunit + 2.0 * xmargin,
		       i * yunit);
    }

  // clear inside
  context->set_source_rgba(0.0, 0.0, 0.0, 1.0);
  context->fill_preserve();
  // draw outline
  context->set_source_rgba(1.0, 1.0, 1.0, 1.0);
  context->stroke();
}

unsigned group_hexagon_mirror::rotate(unsigned n, unsigned m) const
{
  const unsigned r[] = { 1, 2, 3, 4, 5, 0, 11, 6, 7, 8, 9, 10, };
  return r[n % size()];
}

unsigned group_hexagon_mirror::reflect(unsigned n) const
{
  const unsigned r[] = { 6, 7, 8, 9, 10, 11, 0, 1, 2, 3, 4, 5, };
  return r[n % size()];
}

grid<bool> group_hexagon_mirror::transform(const grid<bool> &g,
					   unsigned n) const
{
  // transformation matrix coefficients (times 2)
  const int xx[] = { 2,  1, -1, -2, -1,  1, -2, -1,  1,  2,  1, -1, };
  const int xy[] = { 0, -3, -3,  0,  3,  3,  0, -3, -3,  0,  3,  3, };
  const int yx[] = { 0,  1,  1,  0, -1, -1,  0, -1, -1,  0,  1,  1, };
  const int yy[] = { 2,  1, -1, -2, -1,  1,  2,  1, -1, -2, -1,  1, };

  n = n % size();
  grid<bool> t;
  for (int i=0; i<g.get_rows(); ++i)
    for (int j=0; j<g.get_cols(); ++j)
      {
	coords c = g.get_c0() + make_coords(j, i),
	  r = make_coords((xx[n] * c.x + yx[n] * c.y) / 2,
			  (xy[n] * c.x + yy[n] * c.y) / 2);
	if (g.get(c))
	  t.add(r);
      }
  return t;
}
