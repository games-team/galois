/* main.cc -- main window for falling blocks game.  -*- C++ -*-
   Copyright (C) 2011-2022 Gerardo Ballabio

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#include <sstream>
#include <vector>
#include <gtkmm.h>
#include "board.h"
#include "cube.h"
#include "group.h"
#include "hexagon.h"
#include "main.h"
#include "preferences.h"
#include "square.h"
#include "triangle.h"

bool MainWindow::on_key_press_event(GdkEventKey *event)
{
  if (board.is_game())
    {
      unsigned key = event->keyval;

      // pause
      if (key == (board.get_group()->is_3d() ?
		  preferences.get_key_3d(9) : preferences.get_key_2d(5)))
	{
	  if (pause)
	    {
	      pause = false;
	      board_area.set_pause(false);

	      // resume game
	      timeout.disconnect();
	      delay = 1000 / board.get_speed();
	      timeout = Glib::signal_timeout().connect
		(sigc::mem_fun(*this, &MainWindow::on_timeout_down), delay);
	    }
	  else
	    {
	      pause = true;
	      board_area.set_pause(true);
	      board_area.set_pause_key(gdk_keyval_name(key));
	    }
	}

      if (!pause && !drop && !animation)
	{
	  bool refresh = false;
	  if (board.get_group()->is_3d())
	    {
	      // move in four directions, three rotations, one reflection
	      if (key == preferences.get_key_3d(0))
		refresh = board.left();
	      else if (key == preferences.get_key_3d(1))
		refresh = board.right();
	      else if (key == preferences.get_key_3d(2))
		refresh = board.back();
	      else if (key == preferences.get_key_3d(3))
		refresh = board.front();
	      else if (key == preferences.get_key_3d(4))
		refresh = board.rotate(0);
	      else if (key == preferences.get_key_3d(5))
		refresh = board.rotate(1);
	      else if (key == preferences.get_key_3d(6))
		refresh = board.rotate(2);
	      else if (key == preferences.get_key_3d(7))
		refresh = board.reflect();
	      else if (key == preferences.get_key_3d(8))
		on_game_drop();
	    }
	  else
	    {
	      // move in two directions, one rotation, one reflection
	      if (key == preferences.get_key_2d(0))
		refresh = board.left();
	      else if (key == preferences.get_key_2d(1))
		refresh = board.right();
	      else if (key == preferences.get_key_2d(2))
		refresh = board.rotate(0);
	      else if (key == preferences.get_key_2d(3))
		refresh = board.reflect();
	      else if (key == preferences.get_key_2d(4))
		on_game_drop();
	    }

	  if (refresh)
	    board_area.set_block(board.get_block(),
				 land ? board.get_shadow() : grid<bool>(),
				 board.get_index());
	}
    }

  Gtk::Window::on_key_press_event(event); // default handler
  return true;
}

void MainWindow::on_menu_new()
{
  if (board.is_game())
    return;

  // update actions status
  action_new->set_enabled(false);
  action_stop->set_enabled(true);
  action_prefs->set_enabled(false);
  action_scores->set_enabled(false);

  // select spatial group
  int geometry = preferences.get_geometry();
  bool reflection = preferences.get_reflection();
  const group *g = 0;
  switch (geometry)
    {
    case 0:
      if (reflection)
	g = new group_square_mirror;
      else
	g = new group_square;
      break;
    case 1:
      if (reflection)
	g = new group_hexagon_mirror;
      else
	g = new group_hexagon;
      break;
    case 2:
      if (reflection)
	g = new group_triangle_mirror;
      else
	g = new group_triangle;
      break;
    case 3:
      if (reflection)
	g = new group_cube_mirror;
      else
	g = new group_cube;
      break;
    }

  // board width
  int width = preferences.get_width(), depth = preferences.get_depth();

  // level change mode
  int mode1 = preferences.get_mode1(), mode2 = preferences.get_mode2();

  // speed level
  int speed = preferences.get_speed();

  // number of bricks per block
  int bmax = preferences.get_bricks_max(),
    bmin = preferences.get_bricks_min();

  // empty cells per line
  int empty = preferences.get_empty();

  // preview next block
  next = preferences.get_next();

  // show where block will land
  land = preferences.get_land();

  // start game
  animation = 0;
  drop = pause = false;
  board.start_game(g, width, depth, mode1, mode2, speed, bmin, bmax, empty);
  board_area.start_game(g, board.get_cells(), board.get_block(),
			board.get_index());
  next_area.set_group(g);
  if (next)
    next_area.set_block(board.get_next_block(), board.get_next_index());
  else
    next_area.set_block(grid<bool>(), 0);

  // game statistics
  std::ostringstream o;
  o << "Level: " << board.get_level();
  level_label.set_text(o.str());
  lines_label.set_text("Lines: 0");
  score_label.set_text("Score: 0");

  // start timeout
  delay = 1000 / board.get_speed();
  timeout = Glib::signal_timeout().connect
    (sigc::mem_fun(*this, &MainWindow::on_timeout_down), delay);
}

void MainWindow::on_menu_stop()
{
  if (!board.is_game())
    return;

  board.stop();
  board_area.set_pause(false);
  // set y to negative so that plane isn't drawn in 3D game
  board_area.set_block(grid<bool>(0, 0, 0, 0, -1, 0), grid<bool>(), 0);
  next_area.set_block(grid<bool>(), 0);

  // stop timeout
  timeout.disconnect();

  // update actions status
  action_new->set_enabled(true);
  action_stop->set_enabled(false);
  action_prefs->set_enabled(true);
  action_scores->set_enabled(true);
}

void MainWindow::on_menu_prefs()
{
  preferences.reset_page();
  preferences.show();
}

void MainWindow::on_menu_scores()
{
  // I would place this in the constructor
  // if I could made sure that scores is initialized after preferences
  static bool first = true;
  if (first)
    {
      first = false;
      scores.filter(preferences.get_geometry(), preferences.get_reflection(),
		    preferences.get_width(), preferences.get_depth(),
		    preferences.get_mode1(), preferences.get_mode2(),
		    preferences.get_speed(), preferences.get_bricks_max(),
		    preferences.get_bricks_min(), preferences.get_empty(),
		    preferences.get_next(), preferences.get_land());
    }

  scores.show();
}

void MainWindow::on_menu_quit()
{
  on_menu_stop();
  hide();
}

void MainWindow::on_menu_help()
{
#if GTKMM_MINOR_VERSION >= 22
  // launch help browser
  if (!gtk_show_uri_on_window(static_cast<Gtk::Window *>(this)->gobj(),
			      "ghelp:galois", GDK_CURRENT_TIME, 0))
    // fall back to html manual (requires network connection)
    gtk_show_uri_on_window(static_cast<Gtk::Window *>(this)->gobj(),
			   "http://www.nongnu.org/galois/galois.html",
			   GDK_CURRENT_TIME, 0);
#else
  // launch help browser
  if (!gtk_show_uri(get_screen()->gobj(),
		    "ghelp:galois", GDK_CURRENT_TIME, 0))
    // fall back to html manual (requires network connection)
    gtk_show_uri(get_screen()->gobj(),
		 "http://www.nongnu.org/galois/galois.html",
		 GDK_CURRENT_TIME, 0);
#endif
}

void MainWindow::on_menu_about()
{
  Gtk::AboutDialog dialog;
  dialog.set_transient_for(*this);

  // don't show missing icon, I don't like how it looks
  dialog.set_logo(Gdk::Pixbuf::create(Gdk::COLORSPACE_RGB, true, 8, 1, 1));

  dialog.set_program_name("Galois");
  dialog.set_version("0.8");
  dialog.set_comments("A falling blocks game with blocks of various shapes"
		      " and sizes");
  dialog.set_website("http://www.nongnu.org/galois/");
  dialog.set_copyright("Copyright © 2011-2022 Gerardo Ballabio");
  dialog.set_license
    ("Galois is free software; you can redistribute it and/or modify\n"
     "it under the terms of the GNU General Public License as published by\n"
     "the Free Software Foundation; either version 3 of the License, or\n"
     "(at your option) any later version.\n"
     "\n"
     "Galois is distributed in the hope that it will be useful, but\n"
     "WITHOUT ANY WARRANTY; without even the implied warranty of\n"
     "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n"
     "GNU General Public License for more details.\n"
     "\n"
     "You should have received a copy of the GNU General Public License\n"
     "along with Galois.  If not, see <http://www.gnu.org/licenses/>.");

  dialog.run();
}

void MainWindow::on_response_prefs(int response_id)
{
  if (response_id == Gtk::RESPONSE_DELETE_EVENT)
    preferences.hide();
}

void MainWindow::on_response_scores(int response_id)
{
  if (response_id == Gtk::RESPONSE_DELETE_EVENT)
    scores.hide();
}

bool MainWindow::on_timeout_down()
{
  if (pause)
    timeout.disconnect();
  else if (!board.down())
    on_game_next();
  else
    board_area.set_block(board.get_block(),
			 land ? board.get_shadow() : grid<bool>(),
			 board.get_index());

  return true;
}

bool MainWindow::on_timeout_drop()
{
  if (!board.down())
    {
      // switch timeout
      timeout.disconnect();
      delay = 1000 / board.get_speed();
      timeout = Glib::signal_timeout().connect
	(sigc::mem_fun(*this, &MainWindow::on_timeout_down), delay);

      drop = false;
      on_game_next();
    }
  else
    board_area.set_block(board.get_block(),
			 land ? board.get_shadow() : grid<bool>(),
			 board.get_index());

  return true;
}

bool MainWindow::on_timeout_lines()
{
  if (animation > 0)
    {
      board.color_lines(--animation);
      board_area.queue_draw();
      return true;
    }
  // else

  board.remove_lines();

  // update scores
  std::ostringstream o1, o2, o3;
  o1 << "Level: " << board.get_level();
  level_label.set_text(o1.str());
  o2 << "Lines: " << board.get_lines();
  lines_label.set_text(o2.str());
  o3 << "Score: " << board.get_score();
  score_label.set_text(o3.str());

  // continue game
  timeout.disconnect();
  delay = 1000 / board.get_speed();
  timeout = Glib::signal_timeout().connect
    (sigc::mem_fun(*this, &MainWindow::on_timeout_down), delay);

  // new blocks
  board_area.set_block(board.get_block(),
		       land ? board.get_shadow() : grid<bool>(),
		       board.get_index());
  if (next)
    next_area.set_block(board.get_next_block(), board.get_next_index());

  // check for end of game
  if (!board.is_game())
    on_game_over();

  return true;
}

void MainWindow::on_game_drop()
{
  if (!drop)
    {
      // switch timeout
      timeout.disconnect();
      timeout = Glib::signal_timeout().connect
	(sigc::mem_fun(*this, &MainWindow::on_timeout_drop), 10);

      drop = true;
    }
}

void MainWindow::on_game_next()
{
  // update score
  std::ostringstream o;
  o << "Score: " << board.get_score();
  score_label.set_text(o.str());

  std::vector<int> filled = board.get_filled();
  if (filled.size() > 0)
    {
      // switch timeout
      timeout.disconnect();
      timeout = Glib::signal_timeout().connect
	(sigc::mem_fun(*this, &MainWindow::on_timeout_lines), 20);
      animation = ncolors;

      // refresh, but without active block
      board_area.set_block(grid<bool>(), grid<bool>(), 0);
    }
  else
    {
      // new blocks
      board.next_block();
      board_area.set_block(board.get_block(),
			   land ? board.get_shadow() : grid<bool>(),
			   board.get_index());
      if (next)
	next_area.set_block(board.get_next_block(), board.get_next_index());

      // check for end of game
      if (!board.is_game())
	on_game_over();
    }
}

void MainWindow::on_game_over()
{
  board_area.end_game();
  next_area.set_block(grid<bool>(), 0);

  // stop timeout
  timeout.disconnect();

  // register score
  Glib::ustring name = Glib::get_real_name();
  if (name == "")
    name = Glib::get_user_name();
  Glib::TimeVal timeval;
  timeval.assign_current_time();
  double time = timeval.as_double();
  scores.add(name, time,
	     board.get_score(), board.get_lines(),
	     board.get_level(), board.get_speed(),
	     board.get_bmax(), board.get_bmin(), board.get_bsuper(),
	     preferences.get_geometry(), preferences.get_reflection(),
	     preferences.get_width(), preferences.get_depth(),
	     preferences.get_mode1(), preferences.get_mode2(),
	     preferences.get_speed(),
	     preferences.get_bricks_max(), preferences.get_bricks_min(),
	     preferences.get_empty(),
	     preferences.get_next(), preferences.get_land());
  if (scores.is_active())
    scores.show();

  // update actions status
  action_new->set_enabled(true);
  action_stop->set_enabled(false);
  action_prefs->set_enabled(true);
  action_scores->set_enabled(true);
}

MainWindow::MainWindow()
  : level_label("", 0.0, 0.0), lines_label("", 0.0, 0.0),
    score_label("", 0.0, 0.0), preferences(*this), scores(*this)
{
  add(grid_all);

  // game elements
  grid_all.attach(board_area, 0, 0, 1, 4);
  board_area.set_hexpand(true);
  board_area.set_vexpand(true);
  grid_all.attach(next_area, 1, 0, 1, 1);
  next_area.set_hexpand(true);
  next_area.set_vexpand(true);
  next_area.set_board_area(&board_area);
  grid_all.attach(level_label, 1, 1, 1, 1);
  grid_all.attach(lines_label, 1, 2, 1, 1);
  grid_all.attach(score_label, 1, 3, 1, 1);
  score_label.set_vexpand(true);

  // connect signals
  preferences.signal_response().connect
    (sigc::mem_fun(*this, &MainWindow::on_response_prefs));
  scores.signal_response().connect
    (sigc::mem_fun(*this, &MainWindow::on_response_scores));
}

void MainWindow::on_startup(Glib::RefPtr<Gtk::Application> &app)
{
  // set actions
  action_new =
    add_action("new", sigc::mem_fun(*this, &MainWindow::on_menu_new));
  app->set_accel_for_action("win.new", "<Primary>n");
  action_stop =
    add_action("stop", sigc::mem_fun(*this, &MainWindow::on_menu_stop));
  action_prefs =
    add_action("prefs", sigc::mem_fun(*this, &MainWindow::on_menu_prefs));
  action_scores =
    add_action("scores", sigc::mem_fun(*this, &MainWindow::on_menu_scores));
  add_action("help", sigc::mem_fun(*this, &MainWindow::on_menu_help));
  app->set_accel_for_action("win.help", "F1");
  add_action("about", sigc::mem_fun(*this, &MainWindow::on_menu_about));
  add_action("quit", sigc::mem_fun(*this, &MainWindow::on_menu_quit));
  app->set_accel_for_action("win.quit", "<Primary>q");

  // set initial actions status
  action_stop->set_enabled(false);

  // set menu bar
  Glib::RefPtr<Gio::Menu> menubar = Gio::Menu::create();

  Glib::RefPtr<Gio::Menu> game = Gio::Menu::create();
  game->append("_New", "win.new");
  game->append("_Stop", "win.stop");
  game->append("_Preferences", "win.prefs");
  game->append("_Scores", "win.scores");
  game->append("_Quit", "win.quit");
  menubar->append_submenu("_Game", game);

  Glib::RefPtr<Gio::Menu> help = Gio::Menu::create();
  help->append("_Contents", "win.help");
  help->append("_About", "win.about");
  menubar->append_submenu("_Help", help);

  app->set_menubar(menubar);

  app->add_window(*this);
  show_all();
}

int main(int argc, char *argv[])
{
  Glib::RefPtr<Gtk::Application> app = Gtk::Application::create(argc, argv);

  MainWindow window;
  app->signal_startup().connect
    (sigc::bind<Glib::RefPtr<Gtk::Application> &>
     (sigc::mem_fun(window, &MainWindow::on_startup), app));

  return app->run(window);
}
