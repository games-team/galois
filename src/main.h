/* main.h -- main window for falling blocks game.  -*- C++ -*-
   Copyright (C) 2011-2017 Gerardo Ballabio

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#ifndef __MAIN_
#define __MAIN_

#include <gtkmm.h>
#include "area.h"
#include "board.h"
#include "group.h"
#include "preferences.h"
#include "scores.h"

class MainWindow : public Gtk::ApplicationWindow
{
private:
  // child widgets
  Gtk::Grid grid_all;
  ; BoardArea board_area;
  ; NextArea next_area;
  ; Gtk::Label level_label;
  ; Gtk::Label lines_label;
  ; Gtk::Label score_label;

  // standalone dialogs
  PrefsDialog preferences;
  ScoresDialog scores;

  // menu actions
  Glib::RefPtr<Gio::SimpleAction> action_new, action_stop,
    action_prefs, action_scores;

  // timeout
  sigc::connection timeout;
  int delay; // milliseconds
  int animation;
  bool drop, pause;

  // game logic
  game_board board;
  bool next, land;

protected:
  virtual bool on_key_press_event(GdkEventKey *);
  virtual void on_menu_new();
  virtual void on_menu_stop();
  virtual void on_menu_prefs();
  virtual void on_menu_scores();
  virtual void on_menu_quit();
  virtual void on_menu_help();
  virtual void on_menu_about();
  virtual void on_response_prefs(int response_id);
  virtual void on_response_scores(int response_id);
  virtual bool on_timeout_down();
  virtual bool on_timeout_drop();
  virtual bool on_timeout_lines();
  virtual void on_game_drop();
  virtual void on_game_next();
  virtual void on_game_over();
public:
  MainWindow();
  ~MainWindow() { }

  virtual void on_startup(Glib::RefPtr<Gtk::Application> &app);
};

#endif /* __MAIN_ */
