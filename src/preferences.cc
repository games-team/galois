/* preferences.cc -- preferences dialog for falling blocks game.  -*- C++ -*-
   Copyright (C) 2011-2017 Gerardo Ballabio

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <gtkmm.h>
#include <libxml++/libxml++.h>
#include <libxml++/parsers/textreader.h>
#include "preferences.h"

struct control_key {
  Glib::ustring action;
  unsigned key;
};

const control_key control_keys_2d[] = {
  { "Move left", gdk_keyval_from_name("Left"), },
  { "Move right", gdk_keyval_from_name("Right"), },
  { "Rotate", gdk_keyval_from_name("Up"), },
  { "Reflect", gdk_keyval_from_name("Down"), },
  { "Drop", gdk_keyval_from_name("space"), },
  { "Pause", gdk_keyval_from_name("Escape"), },
};

const control_key control_keys_3d[] = {
  { "Move left", gdk_keyval_from_name("Left"), },
  { "Move right", gdk_keyval_from_name("Right"), },
  { "Move up", gdk_keyval_from_name("Up"), },
  { "Move down", gdk_keyval_from_name("Down"), },
  { "Rotate X", gdk_keyval_from_name("1"), },
  { "Rotate Y", gdk_keyval_from_name("2"), },
  { "Rotate Z", gdk_keyval_from_name("3"), },
  { "Reflect", gdk_keyval_from_name("4"), },
  { "Drop", gdk_keyval_from_name("space"), },
  { "Pause", gdk_keyval_from_name("Escape"), },
};

void PrefsDialog::on_hide()
{
  save();
  Gtk::Dialog::on_hide();
}

bool PrefsDialog::on_delete_event(GdkEventAny *event)
{
  save();
  return Gtk::Dialog::on_delete_event(event);
}

bool PrefsDialog::on_key_press_event(GdkEventKey *event)
{
  Gtk::TreeModel::Row row;
  switch (edit_keys)
    {
    case 2:
      // change 2d control key
      row = *(two_list->get_iter(edit_path));
      row[two_columns.key] = gdk_keyval_name(event->keyval);
      edit_keys = 0;
      return true;

    case 3:
      // change 3d control key
      row = *(three_list->get_iter(edit_path));
      row[three_columns.key] = gdk_keyval_name(event->keyval);
      edit_keys = 0;
      return true;

    default:
      // normal actions
      return Gtk::Dialog::on_key_press_event(event);
    }
}

void PrefsDialog::on_geometry_radio_signal_toggled()
{
  if (get_geometry_3d())
    {
      depth_box.set_sensitive(true);

      const int min = 4, max = 8;
      width_spin.get_adjustment()->set_lower(min);
      width_spin.get_adjustment()->set_upper(max);
      width_spin.set_value(width3);
    }
  else
    {
      depth_box.set_sensitive(false);

      const int min = 10, max = 20;
      width_spin.get_adjustment()->set_lower(min);
      width_spin.get_adjustment()->set_upper(max);
      width_spin.set_value(width2);
    }
}

void PrefsDialog::on_width_spin_signal_value_changed()
{
  if (get_geometry_3d())
    {
      width3 = get_width();
      depth_spin.get_adjustment()->set_upper(width3);
      if (get_depth() > width3)
	depth_spin.set_value(width3);
    }
  else
    width2 = get_width();
}

void PrefsDialog::on_mode1_combo_signal_changed()
{
  mode2_box.set_sensitive(get_mode1() == 1);
}

void PrefsDialog::on_bricks_spin1_signal_value_changed()
{
  int max = get_bricks_max();
  bricks_spin2.get_adjustment()->set_upper(max);
  if (get_bricks_min() > max)
    bricks_spin2.set_value(max);
}

void PrefsDialog::on_two_cell_editing_started(Gtk::CellEditable *cell,
					      const Glib::ustring &path)
{
  edit_keys = 2;
  edit_path = path;
}

void PrefsDialog::on_three_cell_editing_started(Gtk::CellEditable *cell,
						const Glib::ustring &path)
{
  edit_keys = 3;
  edit_path = path;
}

PrefsDialog::PrefsDialog(Gtk::Window &parent)
  : Gtk::Dialog("Preferences", parent, true), // modal dialog
    page_game_box(Gtk::ORIENTATION_VERTICAL),
    geometry_box(Gtk::ORIENTATION_VERTICAL),
    width_box(Gtk::ORIENTATION_HORIZONTAL),
    depth_box(Gtk::ORIENTATION_HORIZONTAL),
    game_box(Gtk::ORIENTATION_VERTICAL),
    mode1_box(Gtk::ORIENTATION_HORIZONTAL),
    mode2_box(Gtk::ORIENTATION_HORIZONTAL),
    speed_box(Gtk::ORIENTATION_HORIZONTAL),
    bricks_box(Gtk::ORIENTATION_HORIZONTAL),
    empty_box(Gtk::ORIENTATION_HORIZONTAL),
    view_box(Gtk::ORIENTATION_VERTICAL),
    page_controls_box(Gtk::ORIENTATION_VERTICAL),
    width2(10), width3(6), edit_keys(0)
{
  get_vbox()->pack_start(notebook);
  notebook.set_border_width(10);

  // first tab
  notebook.append_page(page_game_box, "Game");

  page_game_box.set_border_width(12);
  page_game_box.set_spacing(6);
  page_game_box.pack_start(geometry_heading, Gtk::PACK_SHRINK);
  geometry_heading.set_text("<span weight=\"bold\">Geometry</span>");
  geometry_heading.set_use_markup(true);
  geometry_heading.set_alignment(Gtk::ALIGN_START);
  page_game_box.pack_start(geometry_box, Gtk::PACK_SHRINK);
  geometry_box.set_margin_start(12);
  geometry_box.set_spacing(6);
  for (unsigned i=0; i<sizeof(geometry_radio)/sizeof(geometry_radio[0]); ++i)
    {
      geometry_radio[i].set_group(geometry_group);
      geometry_radio[i].set_label(brick_shape[i].description);
      geometry_box.pack_start(geometry_radio[i]);
      geometry_radio[i].signal_toggled().connect
	(sigc::mem_fun(*this,
		       &PrefsDialog::on_geometry_radio_signal_toggled));
    }
  geometry_radio[0].set_active();
  geometry_box.add(geometry_check);
  geometry_check.set_label("Allow block reflection");
  geometry_box.pack_start(width_box, Gtk::PACK_SHRINK);
  width_box.set_spacing(6);
  width_box.pack_start(width_label, Gtk::PACK_SHRINK);
  width_label.set_text("Board width:");
  width_box.pack_start(width_spin, Gtk::PACK_SHRINK);
  width_spin.set_adjustment(Gtk::Adjustment::create(10, 10, 20));
  width_spin.signal_value_changed().connect
    (sigc::mem_fun(*this, &PrefsDialog::on_width_spin_signal_value_changed));
  width_box.pack_start(depth_box, Gtk::PACK_SHRINK);
  depth_box.set_spacing(6);
  depth_box.pack_start(depth_label, Gtk::PACK_SHRINK);
  depth_label.set_text("depth:");
  depth_box.pack_start(depth_spin, Gtk::PACK_SHRINK);
  depth_spin.set_adjustment(Gtk::Adjustment::create(width3, 4, width3));
  depth_box.set_sensitive(false);

  page_game_box.pack_start(game_heading, Gtk::PACK_SHRINK);
  game_heading.set_text("<span weight=\"bold\">Game</span>");
  game_heading.set_use_markup(true);
  game_heading.set_alignment(Gtk::ALIGN_START);
  game_heading.set_margin_top(12);
  page_game_box.pack_start(game_box, Gtk::PACK_SHRINK);
  game_box.set_margin_start(12);
  game_box.set_spacing(6);
  game_box.pack_start(mode1_box, Gtk::PACK_SHRINK);
  mode1_box.set_spacing(6);
  mode1_box.pack_start(mode1_label, Gtk::PACK_SHRINK);
  mode1_label.set_text("On level change: increase");
  mode1_box.pack_start(mode1_combo, Gtk::PACK_SHRINK);
  mode1_combo.append("speed");
  mode1_combo.append("max. block size");
  mode1_combo.append("superblock size");
  mode1_combo.set_active(0);
  mode1_combo.signal_changed().connect
    (sigc::mem_fun(*this, &PrefsDialog::on_mode1_combo_signal_changed));
  game_box.pack_start(mode2_box, Gtk::PACK_SHRINK);
  mode2_box.set_margin_start(12);
  mode2_box.set_spacing(6);
  mode2_box.pack_start(mode2_label1, Gtk::PACK_SHRINK);
  mode2_label1.set_text("and");
  mode2_box.pack_start(mode2_combo, Gtk::PACK_SHRINK);
  mode2_combo.append("decrease");
  mode2_combo.append("keep fixed");
  mode2_combo.append("increase");
  mode2_combo.set_active(0);
  mode2_box.pack_start(mode2_label2, Gtk::PACK_SHRINK);
  mode2_label2.set_text("min. block size");
  mode2_box.set_sensitive(false);
  game_box.pack_start(speed_box, Gtk::PACK_SHRINK);
  speed_box.set_spacing(6);
  speed_box.pack_start(speed_label, Gtk::PACK_SHRINK);
  speed_label.set_text("Initial speed level:");
  speed_box.pack_start(speed_spin, Gtk::PACK_SHRINK);
  speed_spin.set_adjustment(Gtk::Adjustment::create(5, 1, 9));
  game_box.pack_start(bricks_box, Gtk::PACK_SHRINK);
  bricks_box.pack_start(bricks_label1, Gtk::PACK_SHRINK);
  bricks_box.set_spacing(6);
  bricks_label1.set_text("Initial block size: maximum");
  bricks_box.pack_start(bricks_spin1, Gtk::PACK_SHRINK);
  bricks_spin1.set_adjustment(Gtk::Adjustment::create(4, 4, 8));
  bricks_box.pack_start(bricks_label2, Gtk::PACK_SHRINK);
  bricks_label2.set_text("minimum");
  bricks_box.pack_start(bricks_spin2, Gtk::PACK_SHRINK);
  bricks_spin2.set_adjustment(Gtk::Adjustment::create(4, 2, 4));
  bricks_spin1.signal_value_changed().connect
    (sigc::mem_fun(*this,
		   &PrefsDialog::on_bricks_spin1_signal_value_changed));
  game_box.pack_start(empty_box, Gtk::PACK_SHRINK);
  empty_box.set_spacing(6);
  empty_box.pack_start(empty_label1, Gtk::PACK_SHRINK);
  empty_label1.set_text("Remove lines with up to");
  empty_box.pack_start(empty_spin, Gtk::PACK_SHRINK);
  empty_spin.set_adjustment(Gtk::Adjustment::create(0, 0, 2));
  empty_box.pack_start(empty_label2, Gtk::PACK_SHRINK);
  empty_label2.set_text("empty cells");

  page_game_box.pack_start(view_heading, Gtk::PACK_SHRINK);
  view_heading.set_text("<span weight=\"bold\">View</span>");
  view_heading.set_use_markup(true);
  view_heading.set_alignment(Gtk::ALIGN_START);
  view_heading.set_margin_top(12);
  page_game_box.pack_start(view_box, Gtk::PACK_SHRINK);
  view_box.set_margin_start(12);
  view_box.set_spacing(6);
  view_box.pack_start(next_check, Gtk::PACK_SHRINK);
  next_check.set_label("Preview next block");
  next_check.set_active(true);
  view_box.pack_start(land_check, Gtk::PACK_SHRINK);
  land_check.set_label("Show where the block will land");

  // second tab
  notebook.append_page(page_controls_box, "Controls");

  page_controls_box.set_border_width(12);
  page_controls_box.set_spacing(6);
  page_controls_box.pack_start(two_heading, Gtk::PACK_SHRINK);
  two_heading.set_text("<span weight=\"bold\">2D controls</span>");
  two_heading.set_use_markup(true);
  two_heading.set_alignment(Gtk::ALIGN_START);
  page_controls_box.pack_start(two_tree, Gtk::PACK_SHRINK);
  two_tree.set_margin_start(12);
  two_list = Gtk::ListStore::create(two_columns);
  two_tree.set_model(two_list);
  two_tree.append_column("Action", two_columns.action);
  two_tree.append_column_editable("Key", two_columns.key);
  two_tree.set_headers_visible(false);
  for (unsigned i=0;
       i<sizeof(control_keys_2d)/sizeof(control_keys_2d[0]); ++i)
    {
      Gtk::TreeModel::Row row = *(two_list->append());
      row[two_columns.action] = control_keys_2d[i].action;
      row[two_columns.key] = gdk_keyval_name(control_keys_2d[i].key);
    }
  two_tree.get_column_cell_renderer(1)->signal_editing_started().connect
    (sigc::mem_fun(*this, &PrefsDialog::on_two_cell_editing_started));

  page_controls_box.pack_start(three_heading, Gtk::PACK_SHRINK);
  three_heading.set_margin_top(12);
  three_heading.set_text("<span weight=\"bold\">3D controls</span>");
  three_heading.set_use_markup(true);
  three_heading.set_alignment(Gtk::ALIGN_START);
  page_controls_box.pack_start(three_tree, Gtk::PACK_SHRINK);
  three_tree.set_margin_start(12);
  three_list = Gtk::ListStore::create(three_columns);
  three_tree.set_model(three_list);
  three_tree.append_column("Action", three_columns.action);
  three_tree.append_column_editable("Key", three_columns.key);
  three_tree.set_headers_visible(false);
  for (unsigned i=0;
       i<sizeof(control_keys_3d)/sizeof(control_keys_3d[0]); ++i)
    {
      Gtk::TreeModel::Row row = *(three_list->append());
      row[three_columns.action] = control_keys_3d[i].action;
      row[three_columns.key] = gdk_keyval_name(control_keys_3d[i].key);
    }
  three_tree.get_column_cell_renderer(1)->signal_editing_started().connect
    (sigc::mem_fun(*this, &PrefsDialog::on_three_cell_editing_started));

  // close button
  add_button(Gtk::Stock::CLOSE, Gtk::RESPONSE_DELETE_EVENT);

  // reload saved settings
  load();

  get_vbox()->show_all_children();
}

unsigned PrefsDialog::get_geometry() const
{
  for (unsigned i=0; i<sizeof(geometry_radio)/sizeof(geometry_radio[0]); ++i)
    if (geometry_radio[i].get_active())
      return i;

  // should never get here
  return 0;
}

bool PrefsDialog::get_geometry_3d() const
{
  return brick_shape[get_geometry()].is_3d;
}

unsigned PrefsDialog::get_key_2d(int n) const
{
  Gtk::TreeModel::iterator iter = two_list->children().begin();
  for (int i=0; i<n; ++i)
    ++iter;
  Gtk::TreeModel::Row row = *iter;
  Glib::ustring key = row[two_columns.key];
  return gdk_keyval_from_name(key.c_str());
}

unsigned PrefsDialog::get_key_3d(int n) const
{
  Gtk::TreeModel::iterator iter = three_list->children().begin();
  for (int i=0; i<n; ++i)
    ++iter;
  Gtk::TreeModel::Row row = *iter;
  Glib::ustring key = row[three_columns.key];
  return gdk_keyval_from_name(key.c_str());
}

bool PrefsDialog::load()
{
  try
    {
      // open file
      Glib::ustring file = Glib::build_filename(Glib::get_user_config_dir(),
						"galois", "galois.conf");
      xmlpp::TextReader *reader = new xmlpp::TextReader(file);

      reader->read();
      // skip whitespace and comments
      while (reader->get_node_type() ==
	     xmlpp::TextReader::SignificantWhitespace ||
	     reader->get_node_type() == xmlpp::TextReader::Comment)
	reader->read();

      // document node
      if (reader->get_node_type() != xmlpp::TextReader::Element ||
	  reader->get_name() != "galois_config")
	return false;

      // read inner nodes
      for (;;)
	{
	  reader->read();
	  // skip whitespace and comments
	  while (reader->get_node_type() ==
		 xmlpp::TextReader::SignificantWhitespace ||
		 reader->get_node_type() == xmlpp::TextReader::Comment)
	    reader->read();

	  // end of document node
	  if (reader->get_node_type() == xmlpp::TextReader::EndElement
	      && reader->get_name() == "galois_config")
	    break;

	  if (reader->get_node_type() != xmlpp::TextReader::Element)
	    return false;

	  // read and apply properties
	  Glib::ustring name = reader->get_name();
	  if (name == "keys_2d")
	    {
	      // read inner nodes
	      for (;;)
		{
		  reader->read();
		  // skip whitespace and comments
		  while (reader->get_node_type() ==
			 xmlpp::TextReader::SignificantWhitespace ||
			 reader->get_node_type() ==
			 xmlpp::TextReader::Comment)
		    reader->read();

		  // end of document node
		  if (reader->get_node_type() == xmlpp::TextReader::EndElement
		      && reader->get_name() == "keys_2d")
		    break;

		  if (reader->get_node_type() != xmlpp::TextReader::Element ||
		      reader->get_name() != "control_key")
		    return false;

		  reader->move_to_first_attribute();
		  Glib::ustring action = reader->get_value();
		  reader->move_to_next_attribute();
		  Glib::ustring key = reader->get_value();
		  for (Gtk::TreeModel::iterator
			 i=two_list->children().begin();
		       i!=two_list->children().end(); ++i)
		    {
		      Gtk::TreeModel::Row row = *i;
		      if (action == row[two_columns.action])
			row[two_columns.key] = key;
		    }
		}
	    }
	  else if (name == "keys_3d")
	    {
	      // read inner nodes
	      for (;;)
		{
		  reader->read();
		  // skip whitespace and comments
		  while (reader->get_node_type() ==
			 xmlpp::TextReader::SignificantWhitespace ||
			 reader->get_node_type() ==
			 xmlpp::TextReader::Comment)
		    reader->read();

		  // end of document node
		  if (reader->get_node_type() == xmlpp::TextReader::EndElement
		      && reader->get_name() == "keys_3d")
		    break;

		  if (reader->get_node_type() != xmlpp::TextReader::Element ||
		      reader->get_name() != "control_key")
		    return false;

		  reader->move_to_first_attribute();
		  Glib::ustring action = reader->get_value();
		  reader->move_to_next_attribute();
		  Glib::ustring key = reader->get_value();
		  for (Gtk::TreeModel::iterator
			 i=three_list->children().begin();
		       i!=three_list->children().end(); ++i)
		    {
		      Gtk::TreeModel::Row row = *i;
		      if (action == row[three_columns.action])
			row[three_columns.key] = key;
		    }
		}
	    }
	  else
	    {
	      reader->move_to_first_attribute();
	      if (name == "geometry")
		geometry_radio[std::atoi(reader->get_value().c_str())].
		  set_active();
	      else if (name == "reflection")
		geometry_check.set_active
		  (std::atoi(reader->get_value().c_str()));
	      else if (name == "width2")
		{
		  width2 = std::atoi(reader->get_value().c_str());
		  if (!get_geometry_3d())
		    width_spin.set_value(width2);
		}
	      else if (name == "width3")
		{
		  width3 = std::atoi(reader->get_value().c_str());
		  depth_spin.get_adjustment()->set_upper(width3);
		  if (get_geometry_3d())
		    width_spin.set_value(width3);
		}
	      else if (name == "depth")
		depth_spin.set_value(std::atoi(reader->get_value().c_str()));
	      else if (name == "mode1")
		mode1_combo.set_active
		  (std::atoi(reader->get_value().c_str()));
	      else if (name == "mode2")
		mode2_combo.set_active
		  (std::atoi(reader->get_value().c_str()));
	      else if (name == "speed")
		speed_spin.set_value(std::atoi(reader->get_value().c_str()));
	      else if (name == "bricks_max")
		bricks_spin1.set_value
		  (std::atoi(reader->get_value().c_str()));
	      else if (name == "bricks_min")
		bricks_spin2.set_value
		  (std::atoi(reader->get_value().c_str()));
	      else if (name == "empty")
		empty_spin.set_value(std::atoi(reader->get_value().c_str()));
	      else if (name == "next")
		next_check.set_active(std::atoi(reader->get_value().c_str()));
	      else if (name == "land")
		land_check.set_active(std::atoi(reader->get_value().c_str()));
	    }
	}

      // file was parsed successfully
      return true;
    }
  catch (const std::exception &e) { }

  return false;
}

bool PrefsDialog::save() const
{
  // open file, create directory if doesn't exist
  Glib::ustring
    dir = Glib::build_filename(Glib::get_user_config_dir(), "galois"),
    file = Glib::build_filename(dir, "galois.conf");
  g_mkdir_with_parents(dir.c_str(), 0755);
  std::ofstream os(file.c_str());

  // xml header
  os << "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" << std::endl;

  // document node
  os << "<galois_config>" << std::endl;

  os << "  <geometry value=\"" << get_geometry() << "\" />" << std::endl;
  os << "  <reflection value=\"" << get_reflection() << "\" />" << std::endl;
  os << "  <width2 value=\"" << width2 << "\" />" << std::endl;
  os << "  <width3 value=\"" << width3 << "\" />" << std::endl;
  os << "  <depth value=\"" << get_depth() << "\" />" << std::endl;
  os << "  <mode1 value=\"" << get_mode1() << "\" />" << std::endl;
  os << "  <mode2 value=\"" << get_mode2() << "\" />" << std::endl;
  os << "  <speed value=\"" << get_speed() << "\" />" << std::endl;
  os << "  <bricks_max value=\"" << get_bricks_max() << "\" />" << std::endl;
  os << "  <bricks_min value=\"" << get_bricks_min() << "\" />" << std::endl;
  os << "  <empty value=\"" << get_empty() << "\" />" << std::endl;
  os << "  <next value=\"" << get_next() << "\" />" << std::endl;
  os << "  <land value=\"" << get_land() << "\" />" << std::endl;
  os << "  <keys_2d>" << std::endl;
  for (Gtk::TreeModel::iterator i=two_list->children().begin();
       i!=two_list->children().end(); ++i)
    {
      Gtk::TreeModel::Row row = *i;
      os << "    <control_key action=\"" << row[two_columns.action] << "\""
	 << " key=\"" << row[two_columns.key] << "\" />" << std::endl;
    }
  os << "  </keys_2d>" << std::endl;
  os << "  <keys_3d>" << std::endl;
  for (Gtk::TreeModel::iterator i=three_list->children().begin();
       i!=three_list->children().end(); ++i)
    {
      Gtk::TreeModel::Row row = *i;
      os << "    <control_key action=\"" << row[three_columns.action] << "\""
	 << " key=\"" << row[three_columns.key] << "\" />" << std::endl;
    }
  os << "  </keys_3d>" << std::endl;

  // close document node
  os << "</galois_config>" << std::endl;

  return os.good();
}
