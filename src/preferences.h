/* preferences.h -- preferences dialog for falling blocks game.  -*- C++ -*-
   Copyright (C) 2011-2017 Gerardo Ballabio

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#ifndef __PREFERENCES_
#define __PREFERENCES_

#include <vector>
#include <gtkmm.h>

const struct {
  Glib::ustring description;
  bool is_3d;
} brick_shape[] = {
  { "square bricks", false, },
  { "hexagonal bricks", false, },
  { "triangular bricks", false, },
  { "cubic bricks", true, },
};

class PrefsDialog : public Gtk::Dialog
{
private:
  // child widgets
  Gtk::Notebook notebook;

  // first page
  ; Gtk::Box page_game_box;

  ; ; Gtk::Label geometry_heading;
  ; ; Gtk::Box geometry_box;
  ; ; ; Gtk::RadioButton::Group geometry_group;
  ; ; ; Gtk::RadioButton geometry_radio[sizeof(brick_shape) /
					sizeof(brick_shape[0])];
  ; ; ; Gtk::CheckButton geometry_check;
  ; ; ; Gtk::Box width_box;
  ; ; ; ; Gtk::Label width_label;
  ; ; ; ; Gtk::SpinButton width_spin;
  ; ; ; ; Gtk::Box depth_box;
  ; ; ; ; ; Gtk::Label depth_label;
  ; ; ; ; ; Gtk::SpinButton depth_spin;

  ; ; Gtk::Label game_heading;
  ; ; Gtk::Box game_box;
  ; ; ; Gtk::Box mode1_box;
  ; ; ; ; Gtk::Label mode1_label;
  ; ; ; ; Gtk::ComboBoxText mode1_combo;
  ; ; ; Gtk::Box mode2_box;
  ; ; ; ; Gtk::Label mode2_label1;
  ; ; ; ; Gtk::ComboBoxText mode2_combo;
  ; ; ; ; Gtk::Label mode2_label2;
  ; ; ; Gtk::Box speed_box;
  ; ; ; ; Gtk::Label speed_label;
  ; ; ; ; Gtk::SpinButton speed_spin;
  ; ; ; Gtk::Box bricks_box;
  ; ; ; ; Gtk::Label bricks_label1;
  ; ; ; ; Gtk::SpinButton bricks_spin1;
  ; ; ; ; Gtk::Label bricks_label2;
  ; ; ; ; Gtk::SpinButton bricks_spin2;
  ; ; ; Gtk::Box empty_box;
  ; ; ; ; Gtk::Label empty_label1;
  ; ; ; ; Gtk::SpinButton empty_spin;
  ; ; ; ; Gtk::Label empty_label2;

  ; ; Gtk::Label view_heading;
  ; ; Gtk::Box view_box;
  ; ; ; Gtk::CheckButton next_check;
  ; ; ; Gtk::CheckButton land_check;

  // second page
  ; Gtk::Box page_controls_box;
  ; ; Gtk::Label two_heading;
  ; ; Gtk::TreeView two_tree;
  ; ; ; Glib::RefPtr<Gtk::ListStore> two_list;
  ; ; ; struct ColumnsModel : public Gtk::TreeModel::ColumnRecord
	{
	  Gtk::TreeModelColumn<Glib::ustring> action;
	  Gtk::TreeModelColumn<Glib::ustring> key;

	  ColumnsModel() { add(action); add(key); }
        } two_columns;
  ; ; Gtk::Label three_heading;
  ; ; Gtk::TreeView three_tree;
  ; ; ; Glib::RefPtr<Gtk::ListStore> three_list;
  ; ; ; ColumnsModel three_columns;

  int width2, width3;
  int edit_keys;
  Glib::ustring edit_path;
protected:
  virtual void on_hide();
  virtual bool on_delete_event(GdkEventAny *event);
  virtual bool on_key_press_event(GdkEventKey *);
  virtual void on_geometry_radio_signal_toggled();
  virtual void on_width_spin_signal_value_changed();
  virtual void on_mode1_combo_signal_changed();
  virtual void on_bricks_spin1_signal_value_changed();
  virtual void on_two_cell_editing_started(Gtk::CellEditable *cell,
					   const Glib::ustring &path);
  virtual void on_three_cell_editing_started(Gtk::CellEditable *cell,
					     const Glib::ustring &path);
public:
  PrefsDialog(Gtk::Window &parent);

  void reset_page() { notebook.set_current_page(0); }

  unsigned get_geometry() const;
  bool get_geometry_3d() const;
  bool get_reflection() const { return geometry_check.get_active(); }
  int get_width() const { return width_spin.get_value_as_int(); }
  int get_depth() const { return depth_spin.get_value_as_int(); }
  int get_mode1() const { return mode1_combo.get_active_row_number(); }
  int get_mode2() const { return mode2_combo.get_active_row_number(); }
  int get_speed() const { return speed_spin.get_value_as_int(); }
  int get_bricks_max() const { return bricks_spin1.get_value_as_int(); }
  int get_bricks_min() const { return bricks_spin2.get_value_as_int(); }
  int get_empty() const { return empty_spin.get_value_as_int(); }
  bool get_next() const { return next_check.get_active(); }
  bool get_land() const { return land_check.get_active(); }
  unsigned get_key_2d(int n) const;
  unsigned get_key_3d(int n) const;

  bool load();
  bool save() const;
};

#endif /* __PREFERENCES_ */
