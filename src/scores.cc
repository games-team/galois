/* scores.cc -- preferences dialog for falling blocks game.  -*- C++ -*-
   Copyright (C) 2012-2017 Gerardo Ballabio

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <gtkmm.h>
#include <libxml++/libxml++.h>
#include <libxml++/parsers/textreader.h>
#include "scores.h"

bool ScoresDialog::pass(const std::vector<game>::const_iterator &i) const
{
  // get current filters
  int geometry = geometry_combo.get_active_row_number(),
    mode1 = mode1_combo.get_active_row_number();
  bool reflect_flag = reflect_check.get_active(),
    mode2_flag = mode2_check.get_active(),
    size_flag = size_check.get_active(),
    empty_flag = empty_check.get_active();
  int reflection = reflect_combo.get_active_row_number(),
    mode2 = mode2_combo.get_active_row_number(),
    imax = imax_spin.get_value_as_int(), imin = imin_spin.get_value_as_int(),
    empty = empty_spin.get_value_as_int();

  // apply filters
  if (geometry != 4 && i->geometry != geometry)
    return false;
  if (mode1 != 3 && i->mode1 != mode1)
    return false;
  if (reflect_flag && i->reflection != reflection)
    return false;
  if (mode2_flag && mode1 == 1 && i->mode2 != mode2)
    return false;
  if (size_flag && i->imax != imax)
    return false;
  if (size_flag && i->imin != imin)
    return false;
  if (empty_flag && i->empty != empty)
    return false;

  // passed all filters
  return true;
}

void ScoresDialog::make_list(int active)
{
  list->clear();
  active_game = active;
  active_row = -1;
  visible_rows = 0;

  const int max_rows = 10;
  for (std::vector<game>::const_iterator i=games.begin(); i!=games.end(); ++i)
    {
      if (!pass(i))
	continue;

      Gtk::TreeModel::Row row = *(list->append());
      row[columns.name] = i->name;
      row[columns.score] = i->score;
      row[columns.lines] = i->lines;
      row[columns.level] = i->level;
      row[columns.speed] = i->speed;
      row[columns.bmax] = i->bmax;
      row[columns.bmin] = i->bmin;
      row[columns.bsuper] = i->bsuper;
      if (i - games.begin() == active_game)
	{
	  active_row = visible_rows;
	  row[columns.editable] = true;
	}
      else
	row[columns.editable] = false;

      if (++visible_rows == max_rows)
	break;
    }
}

void ScoresDialog::on_hide()
{
  show_button.set_active(false);
  make_list();
  Gtk::Dialog::on_hide();
}

bool ScoresDialog::on_delete_event(GdkEventAny *event)
{
  show_button.set_active(false);
  make_list();
  return Gtk::Dialog::on_delete_event(event);
}

bool ScoresDialog::on_draw(const Cairo::RefPtr<Cairo::Context> &context)
{
  // request minimal space
  const int min_w = 300, min_h = 100;
  Gdk::Rectangle rectangle;
  tree.get_cell_area(Gtk::TreeModel::Path("0"), *tree.get_column(0),
		     rectangle);
  // some more space for headers and scrollbar
  int h = (rectangle.get_height() + 6) * (visible_rows + 1) + 6, w = min_w;
  if (h < min_h)
    h = min_h;
  int x, y;
  scroll.get_size_request(x, y);
  if (w < x)
    w = x;
  if (h != y || w != x)
    scroll.set_size_request(w, h);

  // set active row (the first time)
  if (active_row >= 0)
    {
      set_focus(tree);
      Gtk::TreeModel::iterator iter = list->children().begin();
      for (int i=0; i<active_row; ++i)
	++iter;
      tree.set_cursor(list->get_path(iter), *tree.get_column(0), true);
      active_row = -1;
    }

  // base widget event handler
  return Gtk::Dialog::on_draw(context);
}

void ScoresDialog::on_cell_edited(const Glib::ustring &path,
				  const Glib::ustring &name)
{
  games[active_game].name = name;
  save();
}

void ScoresDialog::on_show_button_signal_toggled()
{
  if (show_button.get_active())
    {
      check_button.show();
      filters_box.show();
    }
  else
    {
      check_button.hide();
      filters_box.hide();
      resize(get_width(), 1);
    }
}

void ScoresDialog::on_check_button_signal_clicked()
{
  if (reflect_check.get_active() && mode2_check.get_active() &&
      size_check.get_active() && empty_check.get_active())
    {
      reflect_check.set_active(false);
      mode2_check.set_active(false);
      size_check.set_active(false);
      empty_check.set_active(false);
    }
  else
    {
      reflect_check.set_active(true);
      mode2_check.set_active(true);
      size_check.set_active(true);
      empty_check.set_active(true);
    }
}

void ScoresDialog::on_imax_spin_signal_value_changed()
{
  int imax = imax_spin.get_value_as_int();
  imin_spin.get_adjustment()->set_upper(imax);
  if (imin_spin.get_value_as_int() > imax)
    imin_spin.set_value(imax);

  make_list(-1);
}

ScoresDialog::ScoresDialog(Gtk::Window &parent)
  : Gtk::Dialog("Scores", parent, true), // modal dialog
    geometry_label("", Gtk::ALIGN_START), mode1_label("", Gtk::ALIGN_START),
    buttons_box(Gtk::ORIENTATION_HORIZONTAL),
    filters_box(Gtk::ORIENTATION_VERTICAL),
    reflect_box(Gtk::ORIENTATION_HORIZONTAL),
    mode2_box(Gtk::ORIENTATION_HORIZONTAL),
    size_box(Gtk::ORIENTATION_HORIZONTAL),
    empty_box(Gtk::ORIENTATION_HORIZONTAL),
    active_game(-1), active_row(-1)
{
  get_vbox()->set_spacing(6);

  // filters
  get_vbox()->pack_start(grid, Gtk::PACK_SHRINK);
  grid.set_margin_start(4);
  grid.set_column_spacing(6);
  grid.attach(geometry_label, 0, 0, 1, 1);
  geometry_label.set_label("Brick shape:");
  grid.attach(geometry_combo, 1, 0, 1, 1);
  geometry_combo.set_hexpand();
  geometry_combo.append("square");
  geometry_combo.append("hexagonal");
  geometry_combo.append("triangular");
  geometry_combo.append("cubic");
  geometry_combo.append("");
  geometry_combo.set_active(0);
  geometry_combo.signal_changed().connect
    (sigc::mem_fun(*this, &ScoresDialog::on_filter_changed));
  grid.attach(mode1_label, 0, 1, 1, 1);
  mode1_label.set_label("Level change:");
  grid.attach(mode1_combo, 1, 1, 1, 1);
  mode1_combo.set_hexpand();
  mode1_combo.append("speed");
  mode1_combo.append("block size");
  mode1_combo.append("superblock size");
  mode1_combo.append("");
  mode1_combo.set_active(0);
  mode1_combo.signal_changed().connect
    (sigc::mem_fun(*this, &ScoresDialog::on_filter_changed));
  get_vbox()->pack_start(buttons_box, Gtk::PACK_SHRINK);
  buttons_box.set_spacing(6);
  buttons_box.pack_start(show_button, Gtk::PACK_SHRINK);
  show_button.set_label("Show all filters");
  show_button.signal_toggled().connect
    (sigc::mem_fun(*this, &ScoresDialog::on_show_button_signal_toggled));
  buttons_box.pack_start(check_button, Gtk::PACK_SHRINK);
  check_button.set_label("Check/uncheck filters");
  check_button.signal_clicked().connect
    (sigc::mem_fun(*this, &ScoresDialog::on_check_button_signal_clicked));
  get_vbox()->pack_start(filters_box, Gtk::PACK_SHRINK);
  filters_box.set_homogeneous(true);
  filters_box.pack_start(reflect_box);
  reflect_box.set_spacing(6);
  reflect_box.pack_start(reflect_check, Gtk::PACK_SHRINK);
  reflect_check.signal_toggled().connect
    (sigc::mem_fun(*this, &ScoresDialog::on_filter_changed));
  reflect_box.pack_start(reflect_label, Gtk::PACK_SHRINK);
  reflect_label.set_label("Allow block reflection:");
  reflect_box.pack_start(reflect_combo, Gtk::PACK_SHRINK);
  reflect_combo.append("no");
  reflect_combo.append("yes");
  reflect_combo.set_active(0);
  reflect_combo.signal_changed().connect
    (sigc::mem_fun(*this, &ScoresDialog::on_filter_changed));
  filters_box.pack_start(mode2_box);
  mode2_box.set_spacing(6);
  mode2_box.pack_start(mode2_check, Gtk::PACK_SHRINK);
  mode2_check.signal_toggled().connect
    (sigc::mem_fun(*this, &ScoresDialog::on_filter_changed));
  mode2_box.pack_start(mode2_label1, Gtk::PACK_SHRINK);
  mode2_label1.set_label("On level change:");
  mode2_box.pack_start(mode2_combo, Gtk::PACK_SHRINK);
  mode2_combo.append("decrease");
  mode2_combo.append("keep fixed");
  mode2_combo.append("increase");
  mode2_combo.set_active(0);
  mode2_combo.signal_changed().connect
    (sigc::mem_fun(*this, &ScoresDialog::on_filter_changed));
  mode2_box.pack_start(mode2_label2, Gtk::PACK_SHRINK);
  mode2_label2.set_label("min. block size");
  filters_box.pack_start(size_box);
  size_box.set_spacing(6);
  size_box.pack_start(size_check, Gtk::PACK_SHRINK);
  size_check.signal_toggled().connect
    (sigc::mem_fun(*this, &ScoresDialog::on_filter_changed));
  size_box.pack_start(imax_label, Gtk::PACK_SHRINK);
  imax_label.set_label("Initial block size: max");
  size_box.pack_start(imax_spin, Gtk::PACK_SHRINK);
  imax_spin.set_adjustment(Gtk::Adjustment::create(4, 4, 8));
  imax_spin.signal_value_changed().connect
    (sigc::mem_fun(*this, &ScoresDialog::on_imax_spin_signal_value_changed));
  size_box.pack_start(imin_label, Gtk::PACK_SHRINK);
  imin_label.set_label("min");
  size_box.pack_start(imin_spin, Gtk::PACK_SHRINK);
  imin_spin.set_adjustment(Gtk::Adjustment::create(4, 2, 4));
  imin_spin.signal_value_changed().connect
    (sigc::mem_fun(*this, &ScoresDialog::on_filter_changed));
  filters_box.pack_start(empty_box);
  empty_box.set_spacing(6);
  empty_box.pack_start(empty_check, Gtk::PACK_SHRINK);
  empty_check.signal_toggled().connect
    (sigc::mem_fun(*this, &ScoresDialog::on_filter_changed));
  empty_box.pack_start(empty_label1, Gtk::PACK_SHRINK);
  empty_label1.set_label("Remove lines with");
  empty_box.pack_start(empty_spin, Gtk::PACK_SHRINK);
  empty_spin.set_adjustment(Gtk::Adjustment::create(0, 0, 2));
  empty_spin.signal_value_changed().connect
    (sigc::mem_fun(*this, &ScoresDialog::on_filter_changed));
  empty_box.pack_start(empty_label2, Gtk::PACK_SHRINK);
  empty_label2.set_label("empty cells");

  // board
  get_vbox()->pack_start(scroll);
  scroll.set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC);
  scroll.add(tree);
  list = Gtk::ListStore::create(columns);
  tree.set_model(list);
  tree.append_column_editable("Name", columns.name);
  tree.append_column("Score", columns.score);
  tree.append_column("Lines", columns.lines);
  tree.append_column("Level", columns.level);
  tree.append_column("Speed", columns.speed);
  tree.append_column("Max s.", columns.bmax);
  tree.append_column("Min s.", columns.bmin);
  tree.append_column("Super s.", columns.bsuper);
  tree.get_column(0)->add_attribute(*tree.get_column_cell_renderer(0),
				    "editable", columns.editable);

  dynamic_cast<Gtk::CellRendererText *>
    (tree.get_column_cell_renderer(0))->signal_edited().connect
    (sigc::mem_fun(*this, &ScoresDialog::on_cell_edited));

  // close button
  add_button(Gtk::Stock::CLOSE, Gtk::RESPONSE_DELETE_EVENT);

  load();

  get_vbox()->show_all_children();
  check_button.hide();
  filters_box.hide();
}

void ScoresDialog::add(const Glib::ustring &name, double time,
		       int score, int lines, int level, int speed,
		       int bmax, int bmin, int bsuper,
		       int geometry, bool reflection, int width, int depth,
		       int mode1, int mode2, int ispeed, int imax, int imin,
		       int empty, bool next, bool land)
{
  // set filters
  filter(geometry, reflection, width, depth, mode1, mode2,
	 ispeed, imax, imin, empty, next, land);

  // don't use an iterator as the loop index
  // because I'm not sure it would still point to the correct place
  // after an insert or erase
  unsigned pos = 0, top = 0;
  int added = -1;

  // skip scores greater than this, count those which pass the filters
  for (; pos<games.size() && top<10; ++pos)
    {
      if (games[pos].score < score)
	break;
      if (pass(pos))
	++top;
    }

  if (top < 10)
    {
      // new score goes here
      games.insert(games.begin() + pos,
		   make_game(name, time, score, lines, level, speed,
			     bmax, bmin, bsuper, geometry, reflection,
			     width, depth, mode1, mode2, ispeed,
			     imax, imin, empty, next, land));
      added = pos;
      ++pos;
      ++top;
    }

  // count scores which pass the filters, drop those outside the top 10
  for (; pos<games.size(); ++pos)
    if (pass(pos) && ++top > 10)
      {
	games.erase(games.begin() + pos);
	--pos;
      }

  // write to file if anything changed
  if (added >= 0 || top > 10)
    save();

  // show
  make_list(added);
}

void ScoresDialog::filter(int geometry, bool reflection,
			  int width, int depth, int mode1, int mode2,
			  int ispeed, int imax, int imin,
			  int empty, bool next, bool land)
{
  set_geometry(geometry);
  set_mode1(mode1);
  reflect_check.set_active();
  set_reflection(reflection);
  mode2_check.set_active();
  set_mode2(mode2);
  size_check.set_active();
  set_imax(imax);
  set_imin(imin);
  empty_check.set_active();
  set_empty(empty);
}

bool ScoresDialog::load()
{
  try
    {
      // open file
      Glib::ustring file =
	// Debian 6.0 hasn't the three-arguments version
	// keep supporting it for now
	Glib::build_filename(Glib::build_filename(Glib::get_user_data_dir(),
						  "galois"), "galois.res");
      xmlpp::TextReader *reader = new xmlpp::TextReader(file);

      reader->read();
      // skip whitespace and comments
      while (reader->get_node_type() ==
	     xmlpp::TextReader::SignificantWhitespace ||
	     reader->get_node_type() == xmlpp::TextReader::Comment)
	reader->read();

      // document node
      if (reader->get_node_type() != xmlpp::TextReader::Element ||
	  reader->get_name() != "galois_scores")
	return false;

      // read game nodes
      std::vector<game> v;
      for (;;)
	{
	  reader->read();
	  // skip whitespace and comments
	  while (reader->get_node_type() ==
		 xmlpp::TextReader::SignificantWhitespace ||
		 reader->get_node_type() == xmlpp::TextReader::Comment)
	    reader->read();

	  // end of document node
	  if (reader->get_node_type() == xmlpp::TextReader::EndElement
	      && reader->get_name() == "galois_scores")
	    break;

	  if (reader->get_node_type() != xmlpp::TextReader::Element ||
	      reader->get_name() != "game")
	    return false;

	  // read attributes, store game
	  game g = make_game();
	  if (reader->has_attributes())
	    {
	      reader->move_to_first_attribute();
	      do
		{
		  if (reader->get_name() == "name")
		    g.name = reader->get_value();
		  else if (reader->get_name() == "time")
		    g.time = std::atof(reader->get_value().c_str());
		  else if (reader->get_name() == "score")
		    g.score = std::atoi(reader->get_value().c_str());
		  else if (reader->get_name() == "lines")
		    g.lines = std::atoi(reader->get_value().c_str());
		  else if (reader->get_name() == "level")
		    g.level = std::atoi(reader->get_value().c_str());
		  else if (reader->get_name() == "speed")
		    g.speed = std::atoi(reader->get_value().c_str());
		  else if (reader->get_name() == "bmax")
		    g.bmax = std::atoi(reader->get_value().c_str());
		  else if (reader->get_name() == "bmin")
		    g.bmin = std::atoi(reader->get_value().c_str());
		  else if (reader->get_name() == "bsuper")
		    g.bsuper = std::atoi(reader->get_value().c_str());
		  else if (reader->get_name() == "geometry")
		    g.geometry = std::atoi(reader->get_value().c_str());
		  else if (reader->get_name() == "reflection")
		    g.reflection = std::atoi(reader->get_value().c_str());
		  else if (reader->get_name() == "width")
		    g.width = std::atoi(reader->get_value().c_str());
		  else if (reader->get_name() == "depth")
		    g.depth = std::atoi(reader->get_value().c_str());
		  else if (reader->get_name() == "mode1")
		    g.mode1 = std::atoi(reader->get_value().c_str());
		  else if (reader->get_name() == "mode2")
		    g.mode2 = std::atoi(reader->get_value().c_str());
		  else if (reader->get_name() == "ispeed")
		    g.ispeed = std::atoi(reader->get_value().c_str());
		  else if (reader->get_name() == "imax")
		    g.imax = std::atoi(reader->get_value().c_str());
		  else if (reader->get_name() == "imin")
		    g.imin = std::atoi(reader->get_value().c_str());
		  else if (reader->get_name() == "empty")
		    g.empty = std::atoi(reader->get_value().c_str());
		  else if (reader->get_name() == "next")
		    g.next = std::atoi(reader->get_value().c_str());
		  else if (reader->get_name() == "land")
		    g.land = std::atoi(reader->get_value().c_str());
		}
	      while (reader->move_to_next_attribute());
	    }
	  v.push_back(g);
	}

      // file was parsed successfully
      games = v;
      make_list();
      return true;
    }
  catch (const std::exception &e) { }

  return false;
}

bool ScoresDialog::save() const
{
  // open file, create directory if doesn't exist
  Glib::ustring
    dir = Glib::build_filename(Glib::get_user_data_dir(), "galois"),
    file = Glib::build_filename(dir, "galois.res");
  g_mkdir_with_parents(dir.c_str(), 0755);
  std::ofstream os(file.c_str());

  // xml header
  os << "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" << std::endl;

  // document node
  os << "<galois_scores>" << std::endl;

  // games
  for (std::vector<game>::const_iterator i=games.begin();
       i!=games.end() && os.good(); ++i)
    os << "  <game"
       << " name=\"" << i->name << "\""
       << std::fixed << " time=\"" << i->time << "\""
       << " score=\"" << i->score << "\""
       << " lines=\"" << i->lines << "\""
       << " level=\"" << i->level << "\""
       << " speed=\"" << i->speed << "\""
       << " bmax=\"" << i->bmax << "\""
       << " bmin=\"" << i->bmin << "\""
       << " bsuper=\"" << i->bsuper << "\""
       << " geometry=\"" << i->geometry << "\""
       << " reflection=\"" << i->reflection << "\""
       << " width=\"" << i->width << "\""
       << " depth=\"" << i->depth << "\""
       << " mode1=\"" << i->mode1 << "\""
       << " mode2=\"" << i->mode2 << "\""
       << " ispeed=\"" << i->ispeed << "\""
       << " imax=\"" << i->imax << "\""
       << " imin=\"" << i->imin << "\""
       << " empty=\"" << i->empty << "\""
       << " next=\"" << i->next << "\""
       << " land=\"" << i->land << "\""
       << " />" << std::endl;

  // close document node
  os << "</galois_scores>" << std::endl;

  return os.good();
}
