/* scores.h -- scores dialog for falling blocks game.  -*- C++ -*-
   Copyright (C) 2012-2017 Gerardo Ballabio

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#ifndef __SCORES_
#define __SCORES_

#include <vector>
#include <gtkmm.h>

struct game
{
  Glib::ustring name;
  double time;
  int score, lines, level;
  int speed, bmax, bmin, bsuper;
  int geometry;
  bool reflection;
  int width, depth;
  int mode1, mode2;
  int ispeed, imax, imin;
  int empty;
  bool next, land;
};

inline game make_game(const Glib::ustring &name, double time,
		      int score, int lines, int level, int speed,
		      int bmax, int bmin, int bsuper,
		      int geometry, bool reflection, int width, int depth,
		      int mode1, int mode2, int ispeed, int imax, int imin,
		      int empty, bool next, bool land)
{
  game g;
  g.name = name;
  g.time = time;
  g.score = score;
  g.lines = lines;
  g.level = level;
  g.speed = speed;
  g.bmax = bmax;
  g.bmin = bmin;
  g.bsuper = bsuper;
  g.geometry = geometry;
  g.reflection = reflection;
  g.width = width;
  g.depth = (geometry == 3) ? depth : 0;
  g.mode1 = mode1;
  g.mode2 = (mode1 == 1) ? mode2 : 0;
  g.ispeed = ispeed;
  g.imax = imax;
  g.imin = imin;
  g.empty = empty;
  g.next = next;
  g.land = land;
  return g;
}

inline game make_game()
{
  game g;
  g.name = "";
  g.time = 0.0;
  g.score = g.lines = g.level = g.speed = g.bmax = g.bmin =
    g.geometry = g.width = g.depth = g.mode1 = g.mode2 = g.ispeed =
    g.imax = g.imin = g.empty = 0;
  g.reflection = g.next = g.land = false;
  return g;
}

class ScoresDialog : public Gtk::Dialog
{
private:
  // filters
  Gtk::Grid grid;
  ; Gtk::Label geometry_label;
  ; Gtk::ComboBoxText geometry_combo;
  ; Gtk::Label mode1_label;
  ; Gtk::ComboBoxText mode1_combo;
  Gtk::Box buttons_box;
  ; Gtk::ToggleButton show_button;
  ; Gtk::Button check_button;
  Gtk::Box filters_box;
  ; Gtk::Box reflect_box;
  ; ; Gtk::CheckButton reflect_check;
  ; ; Gtk::Label reflect_label;
  ; ; Gtk::ComboBoxText reflect_combo;
  ; Gtk::Box mode2_box;
  ; ; Gtk::CheckButton mode2_check;
  ; ; Gtk::Label mode2_label1, mode2_label2;
  ; ; Gtk::ComboBoxText mode2_combo;
  ; Gtk::Box size_box;
  ; ; Gtk::CheckButton size_check;
  ; ; Gtk::Label imax_label, imin_label;
  ; ; Gtk::SpinButton imax_spin, imin_spin;
  ; Gtk::Box empty_box;
  ; ; Gtk::CheckButton empty_check;
  ; ; Gtk::Label empty_label1, empty_label2;
  ; ; Gtk::SpinButton empty_spin;

  // scores board
  Gtk::ScrolledWindow scroll;
  ; Gtk::TreeView tree;
  ; Glib::RefPtr<Gtk::ListStore> list;
  ; struct ColumnsModel : public Gtk::TreeModel::ColumnRecord
    {
      Gtk::TreeModelColumn<Glib::ustring> name;
      Gtk::TreeModelColumn<unsigned> score;
      Gtk::TreeModelColumn<unsigned> lines;
      Gtk::TreeModelColumn<unsigned> level;
      Gtk::TreeModelColumn<double> time; // for internal use
      Gtk::TreeModelColumn<unsigned> speed;
      Gtk::TreeModelColumn<unsigned> bmax;
      Gtk::TreeModelColumn<unsigned> bmin;
      Gtk::TreeModelColumn<unsigned> bsuper;
      Gtk::TreeModelColumn<bool> editable; // for internal use

      ColumnsModel()
      {
	add(name); add(score); add(lines); add(level); add(time);
	add(speed); add(bmax); add(bmin); add(bsuper); add(editable);
      }
    } columns;
  Gtk::TreeView::Column column;
  Gtk::CellRendererText renderer;

  std::vector<game> games;
  int visible_rows, active_game, active_row;

  void set_geometry(int geometry) { geometry_combo.set_active(geometry); }
  void set_mode1(int mode1) { mode1_combo.set_active(mode1); }
  void set_reflection(bool reflection)
  { reflect_combo.set_active(reflection ? 1 : 0); }
  void set_mode2(int mode2) { mode2_combo.set_active(mode2); }
  void set_imax(int imax) { imax_spin.set_value(imax); }
  void set_imin(int imin) { imin_spin.set_value(imin); }
  void set_empty(int empty) { empty_spin.set_value(empty); }

  bool pass(const std::vector<game>::const_iterator &i) const;
  bool pass(int n) const { return pass(games.begin() + n); }
  void make_list(int active = -1);

protected:
  virtual void on_hide();
  virtual bool on_delete_event(GdkEventAny *event);
  virtual bool on_draw(const Cairo::RefPtr<Cairo::Context> &context);
  virtual void on_cell_edited(const Glib::ustring &path,
			      const Glib::ustring &text);
  virtual void on_filter_changed() { make_list(-1); }
  virtual void on_show_button_signal_toggled();
  virtual void on_check_button_signal_clicked();
  virtual void on_imax_spin_signal_value_changed();
public:
  ScoresDialog(Gtk::Window &parent);

  bool is_active() const { return active_row >= 0; }

  void add(const Glib::ustring &name, double time,
	   int score, int lines, int level, int speed,
	   int bmax, int bmin, int bsuper,
	   int geometry, bool reflection, int width, int depth,
	   int mode1, int mode2, int ispeed, int imax, int imin,
	   int empty, bool next, bool land);
  void filter(int geometry, bool reflection, int width, int depth,
	      int mode1, int mode2, int ispeed, int imax, int imin,
	      int empty, bool next, bool land);
  bool load();
  bool save() const;
};

#endif /* __SCORES_ */
