/* square.cc -- spatial groups for square bricks.  -*- C++ -*-
   Copyright (C) 2011-2020 Gerardo Ballabio

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#include <cmath>
#include <vector>
#include <gtkmm.h>
#include "grid.h"
#include "group.h"
#include "square.h"

unsigned group_square::rotate(unsigned n, unsigned m) const
{
  const unsigned r[] = { 1, 2, 3, 0, };
  return r[n % size()];
}

unsigned group_square::reflect(unsigned n) const
{
  return n % size();
}

std::vector<coords> group_square::neighbors(const coords &c) const
{
  std::vector<coords> v;
  v.push_back(c + make_coords(0, -1));
  v.push_back(c + make_coords(-1, 0));
  v.push_back(c + make_coords(1, 0));
  v.push_back(c + make_coords(0, 1));
  return v;
}

grid<bool> group_square::transform(const grid<bool> &g, unsigned n) const
{
  // rotation matrix coefficients
  const int xx[] = { 1,  0, -1,  0, };
  const int xy[] = { 0, -1,  0,  1, };
  const int yx[] = { 0,  1,  0, -1, };
  const int yy[] = { 1,  0, -1,  0, };

  n = n % size();
  grid<bool> t;
  for (int i=0; i<g.get_rows(); ++i)
    for (int j=0; j<g.get_cols(); ++j)
      {
	coords c = g.get_c0() + make_coords(j, i),
	  r = make_coords(xx[n] * c.x + yx[n] * c.y,
			  xy[n] * c.x + yy[n] * c.y);
	if (g.get(c))
	  t.add(r);
      }
  return t;
}

grid<bool> group_square::center(const grid<bool> &g) const
{
  // find center of mass
  coords s = make_coords(), m = make_coords();
  int n = 0;
  for (int i=0; i<g.get_rows(); ++i)
    for (int j=0; j<g.get_cols(); ++j)
      {
	coords c = g.get_c0() + make_coords(j, i);
	if (g.get(c))
	  {
	    s = s + c;
	    ++n;
	  }
      }
  if (n > 0)
    m = make_coords(-static_cast<int>(std::floor(double(s.x) / n + 0.5)),
		    -static_cast<int>(std::floor(double(s.y) / n + 0.5)));

  grid<bool> c = g;
  c.move(m);
  return c;
}

/*
grid<bool> group_square::center(const grid<bool> &g) const
{
  coords m = -g.get_c0() +
    make_coords(-g.get_cols() / 2, -g.get_rows() / 2, 0);
  grid<bool> c = g;
  c.move(m);
  return c;
}
*/

std::vector<int> group_square::check_lines(grid<int> &g, int n) const
{
  std::vector<int> filled;
  for (int i=0; i<g.get_rows(); ++i)
    {
      // count empty cells
      int empty = 0;
      for (int j=0; j<g.get_cols(); ++j)
	{
	  coords c = g.get_c0() + make_coords(j, i);
	  if (!g.get(c))
	    ++empty;
	}
      if (empty <= n)
	filled.push_back(i);
    }

  return filled;
}

void group_square::color_line(grid<int> &g, int n, int color) const
{
  for (int i=0; i<g.get_cols(); ++i)
    {
      coords c = make_coords(g.get_x0() + i, n);
      if (g.get(c))
	g.set(c, color);
    }
}

void group_square::remove_line(grid<int> &g, int n) const
{
  for (int i=n; i>=0; --i)
    for (int j=0; j<g.get_cols(); ++j)
      {
	coords c = g.get_c0() + make_coords(j, i);
	g.set(c, ((c.y > 0) ? g.get(c + make_coords(0, -1)) : 0));
      }
}

Gdk::Rectangle group_square::block_size(const grid<bool> &b, int size) const
{
  return Gdk::Rectangle(b.get_x0() * size, b.get_y0() * size,
			b.get_cols() * size, b.get_rows() * size);
}

Gdk::Rectangle group_square::board_size(int size, int rows, int cols,
					int layers) const
{
  return Gdk::Rectangle(-(cols + 2) / 2 * size, 0,
			(cols + 2) * size, (rows + 1) * size);
}

void group_square::draw_brick(Cairo::RefPtr<Cairo::Context> context,
			      const coords &c, const Gdk::RGBA &color,
			      int size) const
{
  const double lwidth = 2.0, margin = (lwidth + 1.0) * 0.5;
  context->set_line_width(lwidth);

  context->rectangle(c.x * size + margin, c.y * size + margin,
		     size - 2.0 * margin, size - 2.0 * margin);

  context->set_source_rgba(color.get_red(), color.get_green(),
			   color.get_blue(), 0.8);
  context->fill_preserve();
  context->set_source_rgba(color.get_red(), color.get_green(),
			   color.get_blue(), 1.0);
  context->stroke();
}

void group_square::draw_shadow(Cairo::RefPtr<Cairo::Context> context,
			       const coords &c, const Gdk::RGBA &color,
			       int size) const
{
  const double lwidth = 2.0, margin = (lwidth + 1.0) * 0.5;
  context->set_line_width(lwidth);

  context->rectangle(c.x * size + margin, c.y * size + margin,
		     size - 2.0 * margin, size - 2.0 * margin);

  const double dark = 0.8;
  context->set_source_rgba(color.get_red() * dark, color.get_green() * dark,
			   color.get_blue() * dark, 1.0);
  context->stroke();
}

void group_square::draw_board(Cairo::RefPtr<Cairo::Context> context,
			      int rows, int cols, int layers, int size) const
{
  const double width = 2.0, margin = (width + 1.0) * 0.5;
  context->set_line_width(width);

  int left = -cols / 2, right = left + cols, bottom = rows;

  // left side
  context->move_to(left * size - margin, 0.0);
  context->line_to(left * size - margin, bottom * size + margin);
  // bottom
  context->line_to(right * size + margin, bottom * size + margin);
  // right side
  context->line_to(right * size + margin, 0.0);

  // clear inside
  context->set_source_rgba(0.0, 0.0, 0.0, 1.0);
  context->fill_preserve();
  // draw outline
  context->set_source_rgba(1.0, 1.0, 1.0, 1.0);
  context->stroke();
}

unsigned group_square_mirror::rotate(unsigned n, unsigned m) const
{
  const unsigned r[] = { 1, 2, 3, 0, 7, 4, 5, 6, };
  return r[n % size()];
}

unsigned group_square_mirror::reflect(unsigned n) const
{
  const unsigned r[] = { 4, 5, 6, 7, 0, 1, 2, 3, };
  return r[n % size()];
}

grid<bool> group_square_mirror::transform(const grid<bool> &g,
					  unsigned n) const
{
  // transformation matrix coefficients
  const int xx[] = { 1,  0, -1,  0, -1,  0,  1,  0, };
  const int xy[] = { 0, -1,  0,  1,  0, -1,  0,  1, };
  const int yx[] = { 0,  1,  0, -1,  0, -1,  0,  1, };
  const int yy[] = { 1,  0, -1,  0,  1,  0, -1,  0, };

  n = n % size();
  grid<bool> t;
  for (int i=0; i<g.get_rows(); ++i)
    for (int j=0; j<g.get_cols(); ++j)
      {
	coords c = g.get_c0() + make_coords(j, i),
	  r = make_coords(xx[n] * c.x + yx[n] * c.y,
			  xy[n] * c.x + yy[n] * c.y);
	if (g.get(c))
	  t.add(r);
      }
  return t;
}
