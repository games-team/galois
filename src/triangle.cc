/* triangle.cc -- spatial groups for triangular bricks.  -*- C++ -*-
   Copyright (C) 2011-2020 Gerardo Ballabio

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#include <cmath>
#include <vector>
#include <gtkmm.h>
#include "grid.h"
#include "group.h"
#include "triangle.h"

unsigned group_triangle::rotate(unsigned n, unsigned m) const
{
  const unsigned r[] = { 1, 2, 3, 4, 5, 0, };
  return r[n % size()];
}

unsigned group_triangle::reflect(unsigned n) const
{
  return n % size();
}

int group_triangle::type(const coords &c) const
{
  int x = c.x - static_cast<int>(std::floor(double(c.x) / 6) * 6);
  switch (x)
    {
    case 0: return (c.y % 2 == 0) ? 0 : -1;
    case 1: return (c.y % 2 == 0) ? -1 : 1;
    case 2: return -1;
    case 3: return (c.y % 2 == 0) ? -1 : 0;
    case 4: return (c.y % 2 == 0) ? 1 : -1;
    case 5: return -1;
    }
  // should never get here
  return 0;
}

std::vector<coords> group_triangle::neighbors(const coords &c) const
{
  std::vector<coords> v;
  switch (type(c))
    {
    case 0:
      v.push_back(c + make_coords(1, -1));
      v.push_back(c + make_coords(-2, 0));
      v.push_back(c + make_coords(1, 1));
      break;
    case 1:
      v.push_back(c + make_coords(-1, -1));
      v.push_back(c + make_coords(2, 0));
      v.push_back(c + make_coords(-1, 1));
      break;
    }
  return v;
}

std::vector<coords> group_triangle::blockers(const coords &c) const
{
  std::vector<coords> v;
  switch (type(c))
    {
    case 0: v.push_back(c + make_coords(1, 1));  break;
    case 1: v.push_back(c + make_coords(-1, 1)); break;
    }
  return v;
}

grid<bool> group_triangle::transform(const grid<bool> &g, unsigned n) const
{
  // rotation matrix coefficients (times 2)
  const int xx[] = { 2,  1, -1, -2, -1,  1, };
  const int xy[] = { 0, -1, -1,  0,  1,  1, };
  const int yx[] = { 0,  3,  3,  0, -3, -3, };
  const int yy[] = { 2,  1, -1, -2, -1,  1, };
  // translations
  const int nx[] = { 0, -2, -3, -2, 0, 1, };
  const int ny[] = { 0,  0,  1,  2, 2, 1, };

  n = n % size();
  grid<bool> t;
  for (int i=0; i<g.get_rows(); ++i)
    for (int j=0; j<g.get_cols(); ++j)
      {
	coords c = g.get_c0() + make_coords(j, i),
	  r = make_coords((xx[n] * c.x + yx[n] * c.y) / 2 + nx[n],
			  (xy[n] * c.x + yy[n] * c.y) / 2 + ny[n]);
	if (g.get(c))
	  t.add(r);
      }
  return t;
}

grid<bool> group_triangle::center(const grid<bool> &g) const
{
  // find center of mass
  coords s = make_coords(), m = make_coords();
  int n = 0;
  for (int i=0; i<g.get_rows(); ++i)
    for (int j=0; j<g.get_cols(); ++j)
      {
	coords c = g.get_c0() + make_coords(j, i);
	// change coordinate system
	coords t = make_coords(c.x + 3 * c.y, 3 * c.y - c.x);
	if (g.get(c))
	  {
	    s = s + t;
	    ++n;
	  }
      }
  if (n > 0)
    m = make_coords(-static_cast<int>(std::floor(double(s.x) / n / 6 + 0.5)),
		    -static_cast<int>(std::floor(double(s.y) / n / 6 + 0.5)));
  // change back coordinate system
  coords mm = make_coords((m.x - m.y) * 3, m.x + m.y);

  grid<bool> c = g;
  c.move(mm);
  return c;
}

std::vector<int> group_triangle::check_lines(grid<int> &g, int n) const
{
  std::vector<int> filled;
  for (int i=1; i<g.get_rows(); ++i)
    {
      // count empty cells
      int empty = 0;
      for (int j=0; j<g.get_cols(); ++j)
	{
	  coords c = g.get_c0() + make_coords(j, i);
	  if (type(c) < 0)
	    {
	      c = c + make_coords(0, -1);
	      if (type(c) < 0)
		continue;
	    }
	  if (!g.get(c))
	    ++empty;
	}
      if (empty <= n)
	{
	  filled.push_back(i);
	  ++i; // skip next line
	}
    }

  return filled;
}

void group_triangle::color_line(grid<int> &g, int n, int color) const
{
  for (int i=n-1; i<=n; ++i)
    for (int j=0; j<g.get_cols(); ++j)
      {
	coords c = g.get_c0() + make_coords(j, i);
	if (type(c) >= 0 && g.get(c))
	  g.set(c, color);
      }
}

void group_triangle::remove_line(grid<int> &g, int n) const
{
  for (int i=n; i>=0; --i)
    for (int j=0; j<g.get_cols(); ++j)
      {
	coords c = g.get_c0() + make_coords(j, i);
	if (type(c) >= 0)
	  g.set(c, ((c.y > 1) ? g.get(c + make_coords(0, -2)) : 0));
      }
}

Gdk::Rectangle group_triangle::block_size(const grid<bool> &b, int size) const
{
  double xunit = size * 0.333333333333333, yunit = size * 0.577350269189626;
  int ileft = (b.get_x0() % 3 == 0) ? 0 : -1, iright = 3 - b.get_cols() % 3;
  return Gdk::Rectangle((b.get_x0() + ileft) * xunit,
			b.get_y0() * yunit,
			(b.get_cols() + iright) * xunit,
			(b.get_rows() + 1) * yunit);
}

Gdk::Rectangle group_triangle::board_size(int size, int rows, int cols,
					  int layers) const
{
  double xunit = size * 0.333333333333333;
  return Gdk::Rectangle
    (-static_cast<int>(std::ceil((cols + 6) / 6 * 3 * xunit)), 0,
     static_cast<int>(std::ceil((cols + 6) * xunit)),
     static_cast<int>(std::ceil((rows + 2) * 1.15470053837925152902)) *
     size / 2);
}

void group_triangle::draw_brick(Cairo::RefPtr<Cairo::Context> context,
				const coords &c, const Gdk::RGBA &color,
				int size) const
{
  if (type(c) < 0)
    return;

  const double lwidth = 2.0, margin = (lwidth + 1.0) * 0.5;
  context->set_line_width(lwidth);

  double xunit = size * 0.333333333333333, yunit = size * 0.577350269189626;
  double xmargin = margin, ymargin = margin * 1.732050807568877;
  switch (type(c))
    {
    case 0:
      context->move_to(c.x * xunit + xmargin, c.y * yunit + ymargin);
      context->line_to((c.x + 3) * xunit - 2 * xmargin, (c.y + 1) * yunit);
      context->line_to(c.x * xunit + xmargin, (c.y + 2) * yunit - ymargin);
      break;
    case 1:
      context->move_to((c.x + 2) * xunit - xmargin, c.y * yunit + ymargin);
      context->line_to((c.x - 1) * xunit + 2 * xmargin, (c.y + 1) * yunit);
      context->line_to((c.x + 2) * xunit - xmargin,
		       (c.y + 2) * yunit - ymargin);
      break;
    }
  context->close_path();

  context->set_source_rgba(color.get_red(), color.get_green(),
			   color.get_blue(), 0.8);
  context->fill_preserve();
  context->set_source_rgba(color.get_red(), color.get_green(),
			   color.get_blue(), 1.0);
  context->stroke();
}

void group_triangle::draw_shadow(Cairo::RefPtr<Cairo::Context> context,
				 const coords &c, const Gdk::RGBA &color,
				 int size) const
{
  if (type(c) < 0)
    return;

  const double lwidth = 2.0, margin = (lwidth + 1.0) * 0.5;
  context->set_line_width(lwidth);

  double xunit = size * 0.333333333333333, yunit = size * 0.577350269189626;
  double xmargin = margin, ymargin = margin * 1.732050807568877;
  switch (type(c))
    {
    case 0:
      context->move_to(c.x * xunit + xmargin, c.y * yunit + ymargin);
      context->line_to((c.x + 3) * xunit - 2 * xmargin, (c.y + 1) * yunit);
      context->line_to(c.x * xunit + xmargin, (c.y + 2) * yunit - ymargin);
      break;
    case 1:
      context->move_to((c.x + 2) * xunit - xmargin, c.y * yunit + ymargin);
      context->line_to((c.x - 1) * xunit + 2 * xmargin, (c.y + 1) * yunit);
      context->line_to((c.x + 2) * xunit - xmargin,
		       (c.y + 2) * yunit - ymargin);
      break;
    }
  context->close_path();

  const double dark = 0.8;
  context->set_source_rgba(color.get_red() * dark, color.get_green() * dark,
			   color.get_blue() * dark, 1.0);
  context->stroke();
}

void group_triangle::draw_board(Cairo::RefPtr<Cairo::Context> context,
				int rows, int cols, int layers,
				int size) const
{
  const double width = 2.0, margin = (width + 1.0) * 0.5;
  context->set_line_width(width);

  double xunit = size * 0.333333333333333, yunit = size * 0.577350269189626;
  double xmargin = margin, ymargin = margin * 1.154700538379252;

  int left = (-cols / 6) * 3, right = left + cols, bottom = rows;
  int lparity = ((bottom + left / 3) % 2 == 0) ? 0 : 1,
    rparity = ((bottom + right / 3) % 2 == 0) ? 0 : 1;

  // left side
  context->move_to(left * xunit - xmargin, 0.0);
  context->line_to(left * xunit - margin,
		   (bottom + lparity) * yunit + 0.5 * ymargin);
  // bottom
  for (int i=left; i<=right; i+=3)
    {
      int iparity = ((bottom + i / 3) % 2 == 0) ? 0 : 1;
      context->line_to(i * xunit, (bottom + iparity) * yunit + ymargin);
    }
  // right side
  context->line_to(right * xunit + margin,
		   (bottom + rparity) * yunit + 0.5 * ymargin);
  context->line_to(right * xunit + xmargin, 0.0);

  // clear inside
  context->set_source_rgba(0.0, 0.0, 0.0, 1.0);
  context->fill_preserve();
  // draw outline
  context->set_source_rgba(1.0, 1.0, 1.0, 1.0);
  context->stroke();
}

unsigned group_triangle_mirror::rotate(unsigned n, unsigned m) const
{
  const unsigned r[] = { 1, 2, 3, 4, 5, 0, 11, 6, 7, 8, 9, 10, };
  return r[n % size()];
}

unsigned group_triangle_mirror::reflect(unsigned n) const
{
  const unsigned r[] = { 6, 7, 8, 9, 10, 11, 0, 1, 2, 3, 4, 5, };
  return r[n % size()];
}

grid<bool> group_triangle_mirror::transform(const grid<bool> &g,
					    unsigned n) const
{
  // rotation matrix coefficients (times 2)
  const int xx[] = { 2,  1, -1, -2, -1,  1, -2, -1,  1,  2,  1, -1, };
  const int xy[] = { 0, -1, -1,  0,  1,  1,  0, -1, -1,  0,  1,  1, };
  const int yx[] = { 0,  3,  3,  0, -3, -3,  0, -3, -3,  0,  3,  3, };
  const int yy[] = { 2,  1, -1, -2, -1,  1,  2,  1, -1, -2, -1,  1, };
  // translations
  const int nx[] = { 0, -2, -3, -2, 0, 1, -2, 0, 1, 0, -2, -3, };
  const int ny[] = { 0,  0,  1,  2, 2, 1,  0, 0, 1, 2,  2,  1, };

  n = n % size();
  grid<bool> t;
  for (int i=0; i<g.get_rows(); ++i)
    for (int j=0; j<g.get_cols(); ++j)
      {
	coords c = g.get_c0() + make_coords(j, i),
	  r = make_coords((xx[n] * c.x + yx[n] * c.y) / 2 + nx[n],
			  (xy[n] * c.x + yy[n] * c.y) / 2 + ny[n]);
	if (g.get(c))
	  t.add(r);
      }
  return t;
}
