/* triangle.h -- spatial groups for triangular bricks.  -*- C++ -*-
   Copyright (C) 2011-2020 Gerardo Ballabio

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>. */

#ifndef __TRIANGLE_
#define __TRIANGLE_

#include <vector>
#include <gtkmm.h>
#include "grid.h"

class group_triangle : public group_2d
{
private:
  virtual unsigned size() const { return 6; }
  virtual unsigned rotate(unsigned n, unsigned m) const;
  virtual unsigned reflect(unsigned n) const;
  virtual int type(const coords &c) const;
  virtual std::vector<coords> neighbors(const coords &c) const;
  virtual std::vector<coords> blockers(const coords &c) const;
  virtual grid<bool> transform(const grid<bool> &g, unsigned n) const;
  virtual grid<bool> center(const grid<bool> &g) const;
public:
  virtual unsigned blockset_max() const { return 100; }

  // game
  virtual coords left(const coords &c) const
  { return c + make_coords(-3, (c.y % 2 == 0) ? 1 : -1); }
  virtual coords right(const coords &c) const
  { return c + make_coords(3, (c.y % 2 == 0) ? 1 : -1); }
  virtual coords down(const coords &c) const
  { return c + make_coords(0, 2); }
  virtual grid<int> make_board(int width, int depth = 1) const
  { return grid<int>(34, width * 3, 1, (-width / 2) * 3, 0, 0); }
  virtual std::vector<int> check_lines(grid<int> &g, int n) const;
  virtual void color_line(grid<int> &g, int n, int c) const;
  virtual void remove_line(grid<int> &g, int n) const;

  // drawing methods
  virtual Gdk::Rectangle block_size(const grid<bool> &b, int size) const;
  virtual Gdk::Rectangle board_size(int size, int rows, int cols,
				    int layers) const;
  virtual void draw_brick(Cairo::RefPtr<Cairo::Context> context,
			  const coords &c, const Gdk::RGBA &color,
			  int size) const;
  virtual void draw_shadow(Cairo::RefPtr<Cairo::Context> context,
			   const coords &c, const Gdk::RGBA &color,
			   int size) const;
  virtual void draw_board(Cairo::RefPtr<Cairo::Context> context,
			  int rows, int cols, int layers, int size) const;
};

class group_triangle_mirror : public group_triangle
{
private:
  virtual unsigned size() const { return 12; }
  virtual unsigned rotate(unsigned n, unsigned m) const;
  virtual unsigned reflect(unsigned n) const;
  virtual grid<bool> transform(const grid<bool> &g, unsigned n) const;
public:
  virtual unsigned blockset_max() const { return 80; }
};

#endif /* __TRIANGLE_ */
